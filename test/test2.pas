program ifElse;
function divide(a,b,c,d:integer):real;
begin
	if( ( (a > B) and  (a < C) ) or (a = d)) then
	begin
		divide := a/b;
	end
	else
	begin
		divide := (a/b)/c;
		write('In else:%f\n',divide);
	end;
end;

var 
	a,b,e,d:integer;
	c:real;
begin
	read('%d%d%d%d',@a,@b,@e,@d);
	c := divide(a,b,e,d);
	write('Output is:%f\n',c);
// program functionOverloading;
// var 
//   a,b:real;
//   c:integer;
// begin
//   a :=0.7;
//   // b:=1;
//   // c := 3;
//   // a := b - c;
//   a := -a;
//   write('%f',a);
end.