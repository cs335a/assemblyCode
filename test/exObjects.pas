program exObjects;
type
   Rectangle = object  
   public  
      length, width: integer;
end;
var
  r1: Rectangle;
  
begin
    r1.length :=2;
    r1.width := 3;
    write('In main: %d %d\n', r1.length, r1.width);
end.

