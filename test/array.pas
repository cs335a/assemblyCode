program array1;

type
	vector = array [1..10000] of integer;
var 
	a : vector;
	i:integer;

function callArray(a:vector;num:integer):integer;
var 
	i : integer;
begin
	for i:= 1 to num do
	begin
		a[i] := a[i]*a[i];
		write('%d squared is: %d\n',i,a[i]);
	end;

end;
var 
	num:integer;
begin
	write('Enter a number:');
	read('%d',@num);
	for i := 1 to num do
	begin
		a[i] := i;
	end;
	callArray(a,num);
end.