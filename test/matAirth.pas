program matmult;
type 
	matrix = array [1..100,1..100] of integer;
	matrixF = array[1..100,1..100] of real;
var
	m1,m2,m3:matrix;
	mf:matrixF;
	i,j,dim1,dim2:integer;
function readMatrix(temp1:matrix; dim1, dim2:integer):integer;
var 
	i,temp,j:integer;
begin
	write('Enter the matrix %d %d\n',dim1,dim2);
	for i := 1 to dim1 do
	begin
		for j := 1 to dim2 do 
		begin
			read('%d',@temp);
			temp1[i,j] := temp;
		end;
	end;
end;

function sum(m1,m2,m3:matrix;dim1,dim2:integer):integer;
var 
	i,j:integer;
begin
	for i := 1 to dim1 do
	begin
		for j := 1 to dim2 do
		begin
			m3[i,j] := m1[i,j] + m2[i,j];
		end;
	end;
end;
function sub(m1,m2,m3:matrix;dim1,dim2:integer):integer;
var 
	i,j:integer;
begin
	for i := 1 to dim1 do
	begin
		for j := 1 to dim2 do
		begin
			m3[i,j] := m1[i,j] - m2[i,j];
		end;
	end;
end;
function mult(m1,m2,m3:matrix;dim1,dim2:integer):integer;
var 
	i,j:integer;
begin
	for i := 1 to dim1 do
	begin
		for j := 1 to dim2 do
		begin
			m3[i,j] := m1[i,j] * m2[i,j];
		end;
	end;
end;
function divI(m1,m2,m3:matrix;dim1,dim2:integer):integer;
var 
	i,j:integer;
begin
	for i := 1 to dim1 do
	begin
		for j := 1 to dim2 do
		begin
			m3[i,j] := m1[i,j] div m2[i,j];
		end;
	end;
end;

function print(m:matrix;dim1,dim2:integer):integer;
var i,j:integer;
begin
	for i:=1 to dim1 do
	begin
		for j:= 1 to dim2 do
			write('%d ',m[i,j]);
		write('\n');
	end;
end;
function print(m:matrixf;dim1,dim2:integer):integer;
var i,j:integer;
begin
	for i:=1 to dim1 do
	begin
		for j:= 1 to dim2 do
			write('%d ',m[i,j]);
		write('\n');
	end;
end;

var 
	op:integer;

begin
	write('Give the dimensions of matrices:');
	read('%d%d',@dim1, @dim2);
	readMatrix(m1, dim1, dim2);
	readMatrix(m2, dim1, dim2);
	write('sum is \n');
	sum(m1,m2,m3,dim1,dim2);
	print(m3,dim1,dim2);
	write('difference is \n');
	sub(m1,m2,m3,dim1,dim2);
	print(m3,dim1,dim2);
	write('product is \n');
	mult(m1,m2,m3,dim1,dim2);
	print(m3,dim1,dim2);
	write('divison is \n');
	divI(m1,m2,m3,dim1,dim2);
	print(m3,dim1,dim2);
end.