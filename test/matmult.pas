program matmult;
type 
	matrix = array [1..100,1..100] of integer;

var
	m1,m2,m3:matrix;
	i,j,dim11,dim12,dim22,dim21:integer;
function readMatrix(temp1:matrix; dim1, dim2:integer):integer;
var 
	i,temp,j:integer;
begin
	write('Enter the matrix\n');
	for i := 1 to dim1 do
	begin
		for j := 1 to dim2 do 
		begin
			read('%d',@temp);
			temp1[i,j] := temp;
		end;
	end;
end;

function init(m:matrix):integer;
var 
	i,j:integer;
begin
	for i := 1 to 10 do
	begin
		for j := 1 to 10 do
			m[i,j] := 1; 
	end;
end;

function mult(src1,src2,dest:matrix;a,b,c:integer):integer;
var 
	i,j,k:integer;
begin
	for i:=1 to a do
	begin 
		for j := 1 to c do
		begin
			dest[i,j] := 0;
			for k := 1 to b do
			begin
				dest[i,j] := dest[i,j] + src1[i,k]*src2[k,j];
			end;
		end;
	end;
end;

function print(m:matrix;dim1,dim2:integer):integer;
var i,j:integer;
begin
	for i:=1 to dim1 do
	begin
		for j:= 1 to dim2 do
			write('%d ',m[i,j]);
		write('\n');
	end;
end;

begin
	write('Give the dimensions of matrix 1:');
	read('%d%d',@dim11, @dim12);
	readMatrix(m1, dim11, dim12);
	write('Give the dimensions of matrix 2:');
	read('%d%d',@dim21, @dim22);
	readMatrix(m2, dim21, dim22);
	if(dim12 <> dim21) then
		write('Matrix dimensions do not agree\n')
	else
	begin
		mult(m1,m2,m3,dim11,dim12,dim22);
		print(m3,dim11,dim22);
	end;
end.