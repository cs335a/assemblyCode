program functionOverloading;
var 
  b,c:integer;
  a:integer;
begin
  write('Please enter the two numbers to add:\n');
  read('%d%d',@b,@c);
  write('b:%d c:%d\n',b,c);
  a := (b+c); 
  write('The sum is: %d.\n',a);
end.
