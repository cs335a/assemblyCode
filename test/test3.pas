program Loop;
var
   b,a,i,j,h,length,width: integer;
   c,e: extended;
   array1: array [1..45] of integer;
function for1():integer;
var
   a: integer;
begin
   for a := 10  to 20 do
   begin
      write('value of a: ', a);
   end;
end;

function while1():integer;
var
   a: integer;
begin
   a := 10;
  (* while loop execution *)
   while (((a>3) or (b>5)) and ((a>3) or (b>5))) and ((a>3) or (b>5) )do
   begin
      write('value of a: ', a);
      a:=a +1;
      if( a > 15) then
         (* terminate the loop using break statement *)
          break;
    end;
end;


function nestedPrime():integer;
var
   i, j:integer;
begin
   for i := 2 to 50 do
   begin
      for j := 2 to i do
      begin
         if (i > j ) then
         begin
            break;
         end;
      end; 
   end;
end;

function repeatUntilLoop():integer;
var
   a,b,c,d,e: integer;
   aw:array[1..6,8..90,3..78,98..99] of integer;
begin
   a := 10;
   (* repeat until loop execution *)
   repeat
      write('value of a: ', a);
      a := a + 1;
   until a < 20;
  aw[a+b-d,b*(d +e) ,d*e + b*c,a*b*c*d*e] := 1;
end;

begin
   a := b + 1;
   (* repeat until loop execution *)
   repeat
      if ((((b>5) and (c>=7) ) or ((a>6) and (b<=7))) and ((a>6)or(h>7))) then
      begin
         a := a + 1;
         continue;
      end;
      write('value of a: ', a);
      a := a+1;
   until ( a < 100 );
   a := 3 + array1[4];
end.