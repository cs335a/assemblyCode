program mult;
function mult(a,b:integer):integer;
begin
	mult := a*b;
end;

var a,b,c:integer;
begin
	read('%d%d',@a,@b);
	c := mult(a,b);
	write('The product is:%d\n',c);
end.
