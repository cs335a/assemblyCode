FILE *outFile;	
#define numRegs 6
#define codeBufferLen 100
#define databufferLen 1000
int dataCounter = 0;
char databuffer[databufferLen] = {'\0'};
char codeBuffer[codeBufferLen] = {'\0'};
char indent[] = "\t\t";
#define printIndent (fprintf(outFile, "%s",indent ))
#define printNewLine (fprintf(outFile, "\n" ))
#define currOffset (currSymbolTable->offset)
char msg[] = "msg_";
symTable* currSymbolTable;
gprEntry gprState[numRegs];
tempVarState tempVarArray[threeAClistLen];
int numParameters = 0;
void printCode(char s[]){
	fprintf(outFile, "%s%s\n",indent,s );
}

bool isfloatSrc1,isfloatSrc2,isfloatDest;

void flushBuffer(){
	fprintf(outFile, "%s%s\n",indent,codeBuffer ); 
	codeBuffer[0] = '\0' ;
}

void setTempVar(int tempVarNo){
	if(tempVarArray[tempVarNo].isLive)
		return;
	fprintf(outFile, "%ssub esp, 4\n",indent );	
	tempVarArray[tempVarNo].isLive = true;
	tempVarArray[tempVarNo].stackLocation = currOffset;
	tempVarArray[tempVarNo].location = mem;
	currOffset += 4;
	return;
}


/*void freeTempVar(int tempVarNo){
	tempVarArray[tempVarNo].isLive = false;
}*/

void printArg(ThreeAC_src *var){
	int tempNum;
	switch (var->multiplexer){
		case ACtuple:
			sprintf(codeBuffer, "%sdword [ebp - %d]",codeBuffer,var->tupleSrc->offset + 4 );
			break;
		case ACtemp:
			tempNum = var->intSrc;
			setTempVar(tempNum);
			sprintf(codeBuffer, "%sdword [ebp - %d]",codeBuffer,tempVarArray[tempNum].stackLocation + 4 );
			break;
		case ACint:
			sprintf(codeBuffer, "%s%d",codeBuffer,var->intSrc );
			break;
		case AClabel:
			sprintf(codeBuffer,"%slabel_%d",codeBuffer,var->intSrc);
			break;
		case ACstr:
			sprintf(databuffer,"%s%s%d\tdb\t`%s`,0\n",databuffer,msg,dataCounter,var->strSrc);
			sprintf(codeBuffer,"%sdword %s%d",codeBuffer,msg,dataCounter);
			dataCounter++;
			break;
		case ACread:
			strcat(codeBuffer,"scanf");
			break;
		case ACwrite:
			strcat(codeBuffer,"printf");
			break;
	}
}
int getOffset(ThreeAC_src *var){
	int tempNum;
	switch (var->multiplexer){
		case ACtuple:
			return var->tupleSrc->offset;
			break;
		case ACtemp:
			tempNum = var->intSrc;
			setTempVar(tempNum);
			return tempVarArray[tempNum].stackLocation;
			break;
	}
	return -1;
}


void syncType(ThreeAC_src* dest){
	if(dest->multiplexer == ACtuple){
			dest->type = dest->tupleSrc->type;
	}
	return;
}

void printFloatInst(ThreeAC_src *var){
	int tempNum;
	stringstream ss;
	ss << codeBuffer;
	switch(var->multiplexer){
		case ACtuple:
			if(var->tupleSrc->type.id >= floatType.id && var->tupleSrc->type.id <= doubleType.id){
				ss << "fld ";
			}
			else if(var->tupleSrc->type.id >= shortIntType.id && var->tupleSrc->type.id <= int64Type.id){
				ss << "fild ";
			}
			ss << "dword [ebp - "<< (var->tupleSrc->offset + 4) << "]";
			break;
		case ACtemp:
			tempNum = var->intSrc;
			if(!tempVarArray[tempNum].isLive)
				setTempVar(tempNum);
			if(var->type.id >= floatType.id && var->type.id <= doubleType.id){
				ss << "fld ";
			}
			else if(var->type.id >= shortIntType.id && var->type.id <= int64Type.id){
				ss << "fild ";
			}
			ss << "dword [ebp - "<< (tempVarArray[tempNum].stackLocation + 4) << "]";
			break;
		case ACint:
			ss << "push __float32__(" << var->intSrc << ".0)\n"<< indent << "fld dword [esp]\n" << indent << "add esp, 4";
			break;
		case ACdouble:
			ss << "push __float32__(" << var->doubleSrc << ")\n" <<
			indent << "fld dword [esp]\n" << indent << "add esp, 4";
			break;
	}
	strcpy(codeBuffer, ss.str().c_str());
	flushBuffer();
}

void genAssemblyCode(char *outFileName){
	isfloatSrc1 = isfloatSrc2 = isfloatDest = false;
	int offset1,offset2,length = 0 ;
	outFile = fopen(outFileName,"w");
	if(DEBUG_ASM){
		fprintf(stderr,"outFileName is:%s\n",outFileName);
		outFile = stderr;
	}
	if(outFile == NULL){
		printf("unable to open file %s\n",outFileName );
		abort() ;
	}
	int i;
	for (i = 0; i < nextIndex; ++i)
	{
		if(threeAClist[i].op >= gotoOp && threeAClist[i].op <= ifOp)
			threeAClist[threeAClist[i].dest.intSrc].isLabel = true;
		if(threeAClist[i].op == callOp)
			threeAClist[threeAClist[i].src1.intSrc].isLabel = true;
		if(threeAClist[i].dest.multiplexer == ACtemp){
		//	printf("%d ",threeAClist[i].dest.intSrc);
			(tempVarArray[threeAClist[i].dest.intSrc]).startIndex = i;
		}
		if(threeAClist[i].src1.multiplexer == ACtemp)
			tempVarArray[threeAClist[i].src1.intSrc].endIndex = i;
		if(threeAClist[i].src2.multiplexer == ACtemp)
			tempVarArray[threeAClist[i].src2.intSrc].endIndex = i;	
	}
//	printf("tempCounter:%d\n",tempCounter);
	//push ebp; copy esp into ebp
	symTable* tempTable;
	currSymbolTable = mainTable;
	strcat(databuffer,"SECTION .data\n");
	fprintf(outFile, "extern printf\nextern scanf\nextern stdout\nextern fflush\nSECTION .text\nglobal main\n\n" );
	fprintf(outFile, "main:\n");
	fprintf(outFile, "%spush ebp\n%smov ebp, esp\n%ssub esp, %d\n",indent,indent,indent,currOffset );
	for(i=0;i<nextIndex;i++){
		threeAC tempAC = threeAClist[i];
		if(tempAC.isLabel && tempAC.op != enterOp){
			fprintf(outFile, "\nlabel_%d:\t\n",i);
		}
		syncType(&tempAC.dest);
		syncType(&tempAC.src1);
		syncType(&tempAC.src2);

		if(tempAC.src1.type.id == floatType.id)
			isfloatSrc1 = true;
		else
			isfloatSrc1 = false;
		if(tempAC.src2.type.id == floatType.id)
			isfloatSrc2 = true;
		else
			isfloatSrc2 = false;
		if(tempAC.dest.type.id == floatType.id)
			isfloatDest = true;
		else
			isfloatDest = false;


		switch (tempAC.op){
			case enterMainOp:
				currSymbolTable = mainTable;
				fprintf(outFile, "\nmain_%d:\t",mainIndex);
				break;
			case enterOp:
				fprintf(outFile, "%sjmp main_%d\n\nlabel_%d:\n",indent,mainIndex,i );
				assert(tempAC.dest.multiplexer == ACtuple);
				tempTable = tempAC.dest.tupleSrc->childTable;
				assert(tempTable != NULL);
				currSymbolTable = tempTable;
				fprintf(outFile, "%s:\n%spush ebp\n%smov ebp, esp\n%ssub esp, %d\n",tempAC.dest.tupleSrc->name.c_str(),indent,indent,indent,currOffset );
				if(tempAC.dest.tupleSrc->argList != NULL){
					length = 8;
					for(auto j=tempAC.dest.tupleSrc->argList->begin();j!= tempAC.dest.tupleSrc->argList->end();j++){
						fprintf(outFile,"%smov eax, dword [ebp + %d]\n",indent,length);
						tupleNew* tempTuple = (currSymbolTable->table.find(string(j->name)))->second;
						fprintf(outFile,"%smov dword [ebp - %d], eax\n",indent,tempTuple->offset+4);
						length += 4;
					}
				}
				break;
			case returnOp:
				if(tempAC.dest.multiplexer == ACtuple && tempAC.dest.tupleSrc != NULL){
					fprintf(outFile, "%smov eax, dword [ebp - %d]\n",indent,tempAC.dest.tupleSrc->offset + 4 );
				}
				else
					printCode("mov eax, 0");
				fprintf(outFile, "%smov esp, ebp\n%spop ebp\n%sret\n",indent,indent,indent );
				currSymbolTable = currSymbolTable->parent;
				break;
			case paramOp:
				// float: parameters can be float
				if(tempAC.src1.multiplexer == ACwrite && isfloatDest){
					printFloatInst(&tempAC.dest);
						
					// printCode("sub esp,8");
					// printCode("fstp qword [esp]");
					// numParameters++;
					
					printCode("sub esp, 8");
					printCode("mov eax, esp");
					printCode("fstp qword [eax]");
					printCode("push dword [eax+4]");
					printCode("push dword [eax]");
					numParameters+=2;
				}
				else{
					sprintf(codeBuffer,"push ");
					printArg(&tempAC.dest);
					flushBuffer();
				}
				numParameters++;
				break;
			case callOp:
				// no registers to save
				strcat(codeBuffer,"call ");
				printArg(&tempAC.src1);
				flushBuffer();
				if(numParameters != 0){
					sprintf(codeBuffer,"add esp, %d",numParameters*4);
					flushBuffer();
					numParameters = 0;
				}
				strcat(codeBuffer,"mov ");
				printArg(&tempAC.dest);
				strcat(codeBuffer,", eax");
				flushBuffer();
				break;
			case assignOp:
				if(!isfloatDest){
					//mov src1 into eax
					strcat(codeBuffer, "mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer, "mov " );
					printArg(&tempAC.dest);
					strcat(codeBuffer, ", eax" );
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src1);
					strcat(codeBuffer, "fst ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case plusOp:
				if(!isfloatDest){
					strcat(codeBuffer,"mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer,"add eax, ");
					printArg(&tempAC.src2);
					flushBuffer();
					strcat(codeBuffer,"mov ");
					printArg(&tempAC.dest);
					strcat(codeBuffer,", eax");
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src1);
					printFloatInst(&tempAC.src2);
					strcat(codeBuffer,"fadd");
					flushBuffer();
					strcat(codeBuffer,"fstp ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case minusOp:
				if(!isfloatDest){
					strcat(codeBuffer,"mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer,"sub eax, ");
					printArg(&tempAC.src2);
					flushBuffer();
					strcat(codeBuffer,"mov ");
					printArg(&tempAC.dest);
					strcat(codeBuffer,", eax");
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src1);
					printFloatInst(&tempAC.src2);
					strcat(codeBuffer,"fsub");
					flushBuffer();
					strcat(codeBuffer,"fstp ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case multiplyOp:
				if(!isfloatDest){
					strcat(codeBuffer,"mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer,"imul eax, ");
					printArg(&tempAC.src2);
					flushBuffer();
					strcat(codeBuffer,"mov ");
					printArg(&tempAC.dest);
					strcat(codeBuffer,", eax");
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src1);
					printFloatInst(&tempAC.src2);
					strcat(codeBuffer,"fmul");
					flushBuffer();
					strcat(codeBuffer,"fstp ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case intDivideOp:
				strcat(codeBuffer,"mov edx, 0");
				flushBuffer();
				strcat(codeBuffer,"mov eax, ");
				printArg(&tempAC.src1);
				flushBuffer();
				strcat(codeBuffer,"idiv ");
				if(tempAC.src2.multiplexer == ACint){
					fprintf(outFile,"%spush %d\n",indent,tempAC.src2.intSrc);
					strcat(codeBuffer,"dword [esp]");
					flushBuffer();
					printCode("add esp, 4");
				}
				else{
					printArg(&tempAC.src2);
					flushBuffer();
				}
				strcat(codeBuffer,"mov ");
				printArg(&tempAC.dest);
				strcat(codeBuffer,", eax");
				flushBuffer();
				break;
			case modOp:
				strcat(codeBuffer,"mov edx, 0");
				flushBuffer();
				strcat(codeBuffer,"mov eax, ");
				printArg(&tempAC.src1);
				flushBuffer();
				strcat(codeBuffer,"idiv ");
				printArg(&tempAC.src2);
				flushBuffer();
				strcat(codeBuffer,"mov ");
				printArg(&tempAC.dest);
				strcat(codeBuffer,", edx");
				flushBuffer();
				break;
			case orOp:		// Not tested
				strcat(codeBuffer,"mov eax, ");
				printArg(&tempAC.src1);
				flushBuffer();
				strcat(codeBuffer,"or eax, ");
				printArg(&tempAC.src2);
				flushBuffer();
				strcat(codeBuffer,"mov ");
				printArg(&tempAC.dest);
				strcat(codeBuffer,", eax");
				flushBuffer();
				break;
			case andOp:		// Not tested
				strcat(codeBuffer,"mov eax, ");
				printArg(&tempAC.src1);
				flushBuffer();
				strcat(codeBuffer,"and eax, ");
				printArg(&tempAC.src2);
				flushBuffer();
				strcat(codeBuffer,"mov ");
				printArg(&tempAC.dest);
				strcat(codeBuffer,", eax");
				flushBuffer();
				break;
			case xorOp:		// Not tested
				strcat(codeBuffer,"mov eax, ");
				printArg(&tempAC.src1);
				flushBuffer();
				strcat(codeBuffer,"xor eax, ");
				printArg(&tempAC.src2);
				flushBuffer();
				strcat(codeBuffer,"mov ");
				printArg(&tempAC.dest);
				strcat(codeBuffer,", eax");
				flushBuffer();
				break;
			case notOp:
				strcat(codeBuffer,"not ");
				printArg(&tempAC.src1);
				flushBuffer();
				break;
			case lessOp:
				if(!(isfloatSrc1 || isfloatSrc2)){
					strcat(codeBuffer,"mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer,"cmp eax, ");
					printArg(&tempAC.src2);
					flushBuffer();
					strcat(codeBuffer,"jl ");
					printArg(&tempAC.dest);
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src2);
					printFloatInst(&tempAC.src1);
					strcat(codeBuffer,"fcom");
					flushBuffer();
					strcat(codeBuffer, "jb ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case lessEqualOp:
				if(!(isfloatSrc1 || isfloatSrc2)){
					strcat(codeBuffer,"mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer,"cmp eax, ");
					printArg(&tempAC.src2);
					flushBuffer();
					strcat(codeBuffer,"jle ");
					printArg(&tempAC.dest);
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src2);
					printFloatInst(&tempAC.src1);
					strcat(codeBuffer,"fcom");
					flushBuffer();
					strcat(codeBuffer, "jbe ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case grOp:
				if(!(isfloatSrc1 || isfloatSrc2)){
					strcat(codeBuffer,"mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer,"cmp eax, ");
					printArg(&tempAC.src2);
					flushBuffer();
					strcat(codeBuffer,"jg ");
					printArg(&tempAC.dest);
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src2);
					printFloatInst(&tempAC.src1);
					strcat(codeBuffer,"fcom");
					flushBuffer();
					strcat(codeBuffer, "jnbe ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case grEqualOp:
				if(!(isfloatSrc1 || isfloatSrc2)){
					strcat(codeBuffer,"mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer,"cmp eax, ");
					printArg(&tempAC.src2);
					flushBuffer();
					strcat(codeBuffer,"jge ");
					printArg(&tempAC.dest);
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src2);
					printFloatInst(&tempAC.src1);
					strcat(codeBuffer,"fcom");
					flushBuffer();
					strcat(codeBuffer, "jae ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case equalOp:
				if(!(isfloatSrc1 || isfloatSrc2)){
					strcat(codeBuffer,"mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer,"cmp eax, ");
					printArg(&tempAC.src2);
					flushBuffer();
					strcat(codeBuffer,"je ");
					printArg(&tempAC.dest);
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src2);
					printFloatInst(&tempAC.src1);
					strcat(codeBuffer,"fcom");
					flushBuffer();
					strcat(codeBuffer, "je ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case notEqualOp:
				if(!(isfloatSrc1 || isfloatSrc2)){
					strcat(codeBuffer,"mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer,"cmp eax, ");
					printArg(&tempAC.src2);
					flushBuffer();
					strcat(codeBuffer,"jne ");
					printArg(&tempAC.dest);
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src2);
					printFloatInst(&tempAC.src1);
					strcat(codeBuffer,"fcom");
					flushBuffer();
					strcat(codeBuffer, "jne ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case gotoOp:
				strcat(codeBuffer,"jmp ");
				printArg(&tempAC.dest);
				flushBuffer();
				break;
			case trueOp:
				strcat(codeBuffer,"mov ecx, 1");
				flushBuffer();
				break;
			case falseOp:
				strcat(codeBuffer,"mov ecx, 0");
				flushBuffer();
				break;
			case ifOp:
				strcat(codeBuffer,"cmp ecx, 1");
				flushBuffer();
				strcat(codeBuffer,"je ");
				printArg(&tempAC.dest);
				flushBuffer();
				break;
			case minusUniOp:
				if(!isfloatDest){
					strcat(codeBuffer,"mov eax, ");
					printArg(&tempAC.src1);
					flushBuffer();
					strcat(codeBuffer,"neg eax");
					flushBuffer();
					strcat(codeBuffer,"mov ");
					printArg(&tempAC.dest);
					strcat(codeBuffer,", eax");
					flushBuffer();
				}
				else{
					printFloatInst(&tempAC.src1);
					stringstream ss;
					ss << codeBuffer << "push __float32__(-1.0)\n"<< indent << "fld dword [esp]\n" << indent << "add esp, 4";
					strcpy(codeBuffer,ss.str().c_str());
					flushBuffer();
					strcat(codeBuffer,"fmul");
					flushBuffer();
					strcat(codeBuffer,"fstp ");
					printArg(&tempAC.dest);
					flushBuffer();
					strcat(codeBuffer,"finit");
					flushBuffer();
				}
				break;
			case shiftLeftOp:		// Not checked
				strcat(codeBuffer, "mov eax, ");
				printArg(&tempAC.src1);
				flushBuffer();
				strcat(codeBuffer, "mov ecx, ");
				printArg(&tempAC.src2);
				flushBuffer();
				strcat(codeBuffer,"sal eax, cl");
				flushBuffer();
				strcat(codeBuffer,"mov ");
				printArg(&tempAC.dest);
				strcat(codeBuffer,", eax");
				flushBuffer();				
				break;
			case shiftRightOp:		// Not checked
				strcat(codeBuffer, "mov eax, ");
				printArg(&tempAC.src1);
				flushBuffer();
				strcat(codeBuffer, "mov ecx, ");
				printArg(&tempAC.src2);
				flushBuffer();
				strcat(codeBuffer,"sar eax, cl");
				flushBuffer();
				strcat(codeBuffer,"mov ");
				printArg(&tempAC.dest);
				strcat(codeBuffer,", eax");
				flushBuffer();				
				break;
			case loadOp:		// Not checked
				strcat(codeBuffer,"mov eax, ");
				printArg(&tempAC.src1);
				flushBuffer();
				strcat(codeBuffer,"mov ebx, dword [eax]");
				flushBuffer();
				strcat(codeBuffer,"mov ");
				printArg(&tempAC.dest);
				strcat(codeBuffer,", ebx");
				flushBuffer();				
				break;
			case storeOp:		// Not checked
				strcat(codeBuffer,"mov eax, ");
				printArg(&tempAC.src1);
				flushBuffer();
				strcat(codeBuffer,"mov ebx, ");
				printArg(&tempAC.dest);
				flushBuffer();
				strcat(codeBuffer,"mov dword [ebx], eax");
				flushBuffer();
				break;
			case addrOp:		// Not checked
				strcat(codeBuffer,"mov eax, ebp");
				flushBuffer();
				if(tempAC.src1.multiplexer == ACtemp){
					setTempVar(tempAC.src1.intSrc);
					sprintf(codeBuffer,"%ssub eax, %d",codeBuffer,tempVarArray[tempAC.src1.intSrc].stackLocation + 4 );
				}
				else if(tempAC.src1.multiplexer == ACtuple){
					sprintf(codeBuffer,"%ssub eax, %d",codeBuffer,tempAC.src1.tupleSrc->offset + 4 );
				}
				flushBuffer();
				strcat(codeBuffer,"mov ");
				printArg(&tempAC.dest);
				strcat(codeBuffer,", eax");
				flushBuffer();
				break;		
			case divideOp:
				printFloatInst(&tempAC.src2);
				printFloatInst(&tempAC.src1);
				strcat(codeBuffer,"fdiv st0,st1");		// st0 = st0/st1
				flushBuffer();
				strcat(codeBuffer,"fst ");
				printArg(&tempAC.dest);
				flushBuffer();
				strcat(codeBuffer,"finit");
				flushBuffer();
				break;
			case exitOp:	// Not checked
				printCode("push dword [stdout]");
				printCode("call fflush");
				printCode("add esp, 4");
				fprintf(outFile, "%smov ebx,0\n%smov eax,1\n%sint 0x80\n",indent,indent,indent );
				break;
			case isOp:
				break;
			case dotLoadOp:
				// if(tempAC.src1.multiplexer == ACtuple){
					offset1 = getOffset(&tempAC.src1);
					offset2 = getOffset(&tempAC.src2);
					sprintf(codeBuffer, "mov eax, ebp\n%ssub eax, %d\n%smov ebx, [eax]\n%smov ", indent, offset1+offset2+4, indent, indent);
					printArg(&tempAC.dest);
					strcat(codeBuffer, ", ebx");
					flushBuffer();
				// }
				// else if(tempAC.src1.multiplexer == ACtemp){
				// 	offset2 = getOffset(&tempAC.src2);
				// 	strcat(codeBuffer,"mov eax, ");
				// 	printArg(&tempAC.src1);
				// 	flushBuffer();
				// 	sprintf(codeBuffer,"%ssub eax, %d",codeBuffer,offset2);
				// 	flushBuffer();
				// 	printCode("mov ebx, [eax]");
				// 	strcat(codeBuffer,"mov ");
				// 	printArg(&tempAC.dest);
				// 	strcat(codeBuffer,", ebx");
				// 	flushBuffer();
				// }
				// else{
				// 	assert(false);
				// }
				break;
			case dotStoreOp:
				// if(tempAC.src1.multiplexer == ACtuple){
					offset1 = getOffset(&tempAC.src1);
					offset2 = getOffset(&tempAC.src2);
					sprintf(codeBuffer, "mov eax, ebp\n%ssub eax, %d\n%smov ", indent, offset1+offset2+4, indent);
					printArg(&tempAC.dest);
					strcat(codeBuffer, ", eax");
					flushBuffer();
				// }
				// else if(tempAC.src1.multiplexer == ACtemp){
				// 	offset2 = getOffset(&tempAC.src2);
				// 	strcat(codeBuffer,"mov eax, ");
				// 	printArg(&tempAC.src1);
				// 	flushBuffer();
				// 	sprintf(codeBuffer,"%ssub eax, %d",codeBuffer,offset2);
				// 	flushBuffer();
				// 	strcat(codeBuffer,"mov ");
				// 	printArg(&tempAC.dest);
				// 	strcat(codeBuffer,", eax");
				// 	flushBuffer();
				// }
				// else{
				// 	assert(false);
				// }
				break;
		}
	}

	fprintf(outFile, "\n%s\n",databuffer );
    fclose(outFile);
}

