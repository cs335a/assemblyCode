void emit(ThreeAC_src dest,ThreeAC_src src1,ThreeAC_src src2,opType op);
void printRangeList(arrayRangeList* tempList){
	fprintf(stderr, "%d dimensions\tstart-size:",tempList->size());
	for(auto i = tempList->begin(); i != tempList->end(); i++){
		fprintf(stderr, "%d-%d;", i->start,i->size);
	}
	fprintf(stderr, "\n" );
}
void printType(typeName tempType,const char *name){
	char sep[] = "%%";
	//if(tempType.id > 13)
		fprintf(stderr, "In %s\t%s\tnumBytes:%d\tid:%d\tlevel:%d\tlength:%d\thasRangeList:%d\t%s\t",name,sep,tempType.numBytes,tempType.id,tempType.level,tempType.length,tempType.hasRangeList,sep );
	if(tempType.hasRangeList){
		printRangeList(tempType.rangeList);
	}
	fprintf(stderr, "\n" );
}
// typeName getSimpleMax(typeName a, typeName b){
//     if( a.id<=doubleType.id && a.id != typeError.id && b.id != typeError.id && b.id<=doubleType.id && a.level == b.level){
//         return (a.id>b.id)?a:b;
//     }
//     else if(a.id == b.id && a.level == b.level){
//         return a;
//     }
//     return typeError;
// }
void print3AClist();
typeName validType(typeName a, typeName b, opType op){
	if(a.id == typeError.id || b.id == typeError.id){
		return typeError;
	}
	switch(op){
		case plusOp:
		case minusOp:
		case multiplyOp:
			if(a.level != 0 || b.level!=0){
				return typeError;
			}
			if(a.id > doubleType.id || b.id > doubleType.id ){
				return typeError;
			}
			return (a.id>b.id)?a:b;
			break;
		case divideOp:
			if(a.level != 0 || b.level!=0){
				return typeError;
			}
			if(a.id > doubleType.id || b.id > doubleType.id ){
				return typeError;
			}
			return doubleType;
			break;
		case assignOp:
			if(a.level != b.level){
				return typeError;
			}
			if(a.id == b.id){
				return a;
			}
			if(a.level!=0){
				return typeError;
			}
			if(a.id > doubleType.id || b.id > doubleType.id ){
				return typeError;
			}
			if(a.id > b.id){
				return a;
			}
			if(shortIntType.id<=a.id && int64Type.id>=a.id && shortIntType.id<=b.id && int64Type.id>=b.id){
				return a;
			}
			if(floatType.id<=a.id && doubleType.id>=a.id && floatType.id<=b.id && doubleType.id>=b.id){
				return a;
			}
			break;
		case orOp:
		case andOp:
		case xorOp:
			if(a.id == booleanType.id && b.id == booleanType.id){
				return booleanType;
			}
			break;
		case lessOp:
		case grOp:
		case lessEqualOp:
		case grEqualOp:
		case equalOp:
		case notEqualOp:
			if(a.id > doubleType.id || b.id > doubleType.id || a.level!=0 ||b.level!=0){
				return typeError;
			}
			return booleanType;
			break;
		case intDivideOp:
			if(a.level != 0 || b.level!=0){
				return typeError;
			}
			if(a.id > doubleType.id || b.id > doubleType.id ){
				return typeError;
			}
			return int64Type;
			break;
		case modOp:
			if(a.level != 0 || b.level!=0){
				return typeError;
			}
			if(a.id > doubleType.id || b.id > int64Type.id ){
				return typeError;
			}
			return int64Type;
			break;
		default:
			return a;
	}
	return typeError;
}

void printTypeList(typeMap* tempTypeList ){
	for(auto elem: *tempTypeList){
		printType(elem.second,(elem.first).c_str());
	}
}
void printListType(listType* l){
	for(auto i=l->begin(); i!=l->end();i++){
		cout << *i << " ";
	}
	cout << endl;
}
void printSymTable(symTable* tempTable){
	char sep[] = "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";
	fprintf(stderr,"Symbol table:");
	cout << sep << endl;
	for(auto elem : tempTable->table)
	{
		cout << "name:" << elem.first << "\tnumBytes:" << elem.second->type.numBytes << "\toffset:" << elem.second->offset << "\ttypeName:"  << elem.second->type.name << "\tLevel:" << elem.second->type.level  << endl;
	}
	cout << "\toffset is: " << tempTable->offset << endl;
	printTypeList(tempTable->typeList);
	cout << sep << endl;
}
void throwError(errorCategory error, char* name){
	switch (error){
		case redefineError:
			fprintf(stderr,"ERROR at line:%d definition for variable \"%s\" already exists\n",lineNo,name );
			break;
		case redefineType:
			fprintf(stderr,"ERROR at line:%d a type with name \"%s\" already exists\n",lineNo,name );
			break;
		case undefinedVar:
			fprintf(stderr,"ERROR at line:%d, undefined variable \"%s\".\n",lineNo,name );
			break;
		case undefinedType:
			fprintf(stderr,"ERROR at line:%d, undefined type \"%s\".\n",lineNo,name );
			break;
		case enumIndexOverflow:
			fprintf(stderr, "ERROR at line:%d, index assigned to enum:\"%s\" is smaller than the number of earlier elements\n", lineNo,name);
			break;
		case typeMismatch:
			fprintf(stderr, "ERROR at line:%d, type Mismatch for \"%s\"\n",lineNo,name );
			break;
		case arrayIndexOverflow:
			fprintf(stderr, "ERROR at line:%d for \"%s\" array Index overflowing\n",lineNo,name );
			break;
		case InvalidFunctionDefinition:
			fprintf(stderr,"ERROR at line:%d for \"%s\" Invalid function declaration\n",lineNo,name);
			break;
		case InvalidConstructorDefinition:
			fprintf(stderr,"ERROR at line:%d for \"%s\" Invalid constructor declaration\n",lineNo,name);
			break;
		case InvalidDestructorDefinition:
			fprintf(stderr,"ERROR at line:%d for \"%s\" Invalid destructor declaration\n",lineNo,name);
			break;
		case structuredTypeInDeclaration:
			fprintf(stderr,"ERROR at line:%d for \"%s\" Structured type in declaration\n",lineNo,name);
			break;
	}
	gotError = true;
	return;
}
void throwError(errorCategory error){
	switch (error){
		case redefineError:
			fprintf(stderr,"ERROR at line:%d definition for variable already exists\n",lineNo );
			break;
		case redefineType:
			fprintf(stderr,"ERROR at line:%d a type already exists\n",lineNo );
			break;
		case undefinedVar:
			fprintf(stderr,"ERROR at line:%d, undefined variable\n",lineNo);
			break;
		case undefinedType:
			fprintf(stderr,"ERROR at line:%d, undefined type.\n",lineNo);
			break;
		case typeMismatch:
			fprintf(stderr, "ERROR at line:%d, type Mismatch\n",lineNo );
			break;
		case arrayIndexOverflow:
			fprintf(stderr, "ERROR at line:%d array Index overflowing\n",lineNo );
			break;
		case InvalidFunctionDefinition:
			fprintf(stderr,"ERROR at line:%d for \"%s\" Invalid function declaration\n",lineNo);
			break;
	}
	gotError = true;
	return;
}
bool exists(char* name){
	symTable* table = symStack.top();
	while(table != NULL){
		if(table->table.count(string(name))){
	  if(DEBUG)
		  fprintf(stderr, "Exiting exists for %s returnVal:true\n",name );
			return true;
		}
		table = table->parent;
	}
	if(DEBUG)
		fprintf(stderr, "Exiting exists for %s returnVal:false\n",name );
	return false;
}
tupleNew* insertTuple(typeName type, char* name){
	if(currSymTable->table.count(string(name))){
		throwError(redefineError,name);
		return findTuple(name);
	}
	tupleNew* tempTuple = new tupleNew;
	tempTuple->type = type;
	tempTuple->name = string(name);
	int length;
	if(type.hasRangeList)
		length = type.length+1;
	else
		length = 1;
	int numBytes = type.numBytes*length;
	if(numBytes%4 == 0)
		numBytes = 4;
	if((currSymTable->offset % numBytes) != 0){
		currSymTable->offset = (int)(currSymTable->offset/numBytes + 1)*numBytes;
	}
	tempTuple->offset = currSymTable->offset;
	currSymTable->table.insert(pair<string,tupleNew*>(string(name),tempTuple));
	currSymTable->offset += type.numBytes*length;
	if(DEBUG){
		fprintf(stderr, "Printing symTable after inserting:%s with type:%s\n",name,type.name );
		printSymTable(currSymTable);
	}
	if(type.hasRangeList && !inFunction){
		tempThreeAC.multiplexer = ACtuple;
		tempThreeAC.tupleSrc = tempTuple;
		emit(tempThreeAC,tempThreeAC,nullThreeAC,addrOp);
	}
	return tempTuple;
}

char* concat(char* name, argumentList* argList){
	if(argList == NULL)
		return name;
	char temp[1000] = {'\0'};
	strcat(temp,name);
	for(auto i = argList->begin(); i != argList->end() ; i++){
		sprintf(temp,"%s%s%d",temp,delem,i->type.id);
	}
	if(DEBUG)
		fprintf(stderr,"in concat argument-- final name is:%s\n",temp);
	return strdup(temp);
}

char* concat(char* name, paramList* tempParamList){
	if(tempParamList == NULL)
		return name;
	char temp[1000] = {'\0'};
	strcat(temp,name);
	for(auto i = tempParamList->begin(); i != tempParamList->end() ; i++){
		sprintf(temp,"%s%s%d",temp,delem,i->type.id);
	}
	if(DEBUG)
		fprintf(stderr,"in concat parameter-- final name is:%s\n",temp);
	return strdup(temp);
}

tupleNew* insertTuple(typeName type, char* name,argumentList* argList){
	char* newName;
	newName = concat(name,argList);
	if(currSymTable->table.count(string(newName))){
		throwError(redefineError,newName);
		return findTuple(newName);
	}
	
	symTable* tempTable = new symTable;
	tempTable->offset = 0;
	tempTable->parent = currSymTable;
	tempTable->typeList = new typeMap;
	tempTable->returnVar = NULL;
	
	tupleNew* tempTuple = new tupleNew;
	tempTuple->type = type;
	tempTuple->name = string(newName);
	if(!type.isObject)
		tempTuple->startIndex = nextIndex;
	tempTuple->argList = argList;
	tempTuple->childTable = tempTable;

	int numBytes = type.numBytes;
	if(numBytes%4 == 0) //converting size 8 to size 4
		numBytes = 4;
	if((currSymTable->offset % numBytes) != 0){
		currSymTable->offset = (int)(currSymTable->offset/numBytes + 1)*numBytes;
	}
	tempTuple->offset = currSymTable->offset;
	currSymTable->table.insert(pair<string,tupleNew*>(string(newName),tempTuple));
	currSymTable->offset += type.numBytes;		
	if(DEBUG)
		printSymTable(currSymTable);

	symStack.push(tempTable);
	inFunction = true;
	if(type.id != constructorType.id && type.id != destructorType.id)
		currSymTable->returnVar = insertTuple(type,name);
	if(argList != NULL){
		for(auto i = argList->begin(); i != argList->end();i++){
			insertTuple(i->type,i->name);
		}
	}
	inFunction = false;
	if(DEBUG){
		fprintf(stderr, "Printing symTable after inserting:%s with type:%s\n",newName,type.name );
		printSymTable(currSymTable);
	}
	if(type.isObject)
		symStack.pop();
	return tempTuple;
}
tupleNew* findTuple(char*name){
	symTable* table = symStack.top();
	while(table != NULL){
		auto it = table->table.find(string(name));
		if(it == table->table.end()){
			table = table->parent;
		}
		else
			return it->second;
	}
	throwError(undefinedVar,name);
	return NULL;
}
typeName insertType(char* name,int numBytes, int level){
	if(currSymTable->typeList->count(string(name))){
		throwError(redefineType,name);
		return findType(name);
	}
	if(DEBUG && typeCounter > 15)
		printSymTable(currSymTable);
	typeName tempType = typeError;
	tempType.numBytes = numBytes;
	tempType.name = name;
	tempType.id = typeCounter++;
	tempType.level = level;
	currSymTable->typeList->insert(typePair(string(name),tempType));
	return tempType;
}
typeName insertType(char* name, typeName tempType){
	if(currSymTable->typeList->count(string(name))){
		throwError(redefineType,name);
		return findType(name);
	}
	if(tempType.isObject)
		tempType.id = typeCounter++;
	else
		tempType.id = tempType.id;
	tempType.name = name;
	if(DEBUG)
		fprintf(stderr, "Inserting \"%s\"\n",name );
	currSymTable->typeList->insert(typePair(string(name),tempType));
	if(DEBUG)
		printSymTable(currSymTable);
	return tempType;    	
}
typeName findType(char* name){
	symTable* table = symStack.top();
	while(table != NULL){
		auto it = table->typeList->find(string(name));
		if(it == table->typeList->end())
			table = table->parent;
		else
			return it->second;
	}
	throwError(undefinedType,name);
	return typeError;
}
void calcLengthAndLevel(arrayRangeList* tempRangeList,int* level,int* length){
	int size = *level =  tempRangeList->size();
	int totalSize = 1;
	if(size > 0){
		for(auto i = tempRangeList->begin(); i != tempRangeList->end(); i++ ){
			if(DEBUG)
				fprintf(stderr, "In typeDeclaration:currSize:%d\ttotalSize:%d\n",i->size,totalSize );
			totalSize *= i->size;
		}
		*length = totalSize;
	}
}
void assignVal(ThreeAC_src* dest,ThreeAC_src src){
	dest->multiplexer = src.multiplexer;
	switch (src.multiplexer) {
		case ACdouble:
			dest->doubleSrc = src.doubleSrc;
			break;
		case ACint:
		case AClabel:
			dest->intSrc = src.intSrc;
			break;
		case ACtemp:
			dest->intSrc = src.intSrc;
			dest->type = src.type;
			break;
		case ACstr:
			dest->strSrc = src.strSrc;
			break;
		case ACtuple:
			dest->tupleSrc = src.tupleSrc;
			break;
	}
}
void emit(ThreeAC_src dest,ThreeAC_src src1,ThreeAC_src src2,opType op){	// dest = src1 op src2 -- dest = src1
	threeAClist[nextIndex].op = op;
	assignVal(&(threeAClist[nextIndex].dest),dest);
	assignVal(&(threeAClist[nextIndex].src2),src2);
	assignVal(&(threeAClist[nextIndex].src1),src1);
	nextIndex++;
	if(DEBUG)
		print3AClist();
}
void mergeList(List dest,List src){
	for(auto it = src->begin();it != src->end();it++){
		int temp = *it;
		dest->push_back(temp);
	}
	return;
}
/*    int emitLabel(){
	threeAClist[nextIndex].dest.intSrc = nextIndex;
	threeAClist[nextIndex].dest.multiplexer = AClabel;
	threeAClist[nextIndex].src1 = nullThreeAC;
	threeAClist[nextIndex].src2 = nullThreeAC;
	threeAClist[nextIndex].op = labelOp;
	nextIndex++;
	print3AClist();
	return nextIndex-1;
}*/
int newTmp(){
	return tempCounter++;
}
List backpatch(int label,List tempList){
	if(tempList == NULL){
		tempList = new listType;
		return tempList;
	}
	for(auto i = tempList->begin(); i != tempList->end();i++){
		if(DEBUG)
			cout << "IN backpatch -- patching:" << *i << "\twith:" << label << endl;
		threeAClist[*i].dest.intSrc = label;
		threeAClist[*i].dest.multiplexer = AClabel;
	}
	delete tempList;
	tempList = new listType;
	if(DEBUG)
		printf("Exiting backpatch\n");
	return tempList;
}
void printReg(ThreeAC_src a){
	char quotes = '"';
	switch (a.multiplexer) {
		case ACtuple:
			cout << a.tupleSrc->name << tab;
			break;
		case ACint:
		case AClabel:
			cout << a.intSrc << tab;
			break;
		case ACdouble:
			cout << a.doubleSrc << tab;
			break;
		case ACtemp:
			cout << a.intSrc << "t\t";
			break;
		case ACstr:
			cout << quotes << a.strSrc << quotes << tab;
			break;
		case ACread:
			cout << "read\t";
		case ACwrite:
			cout << "write\t";
	}
}
void printOp(opType op){
	switch (op){
		case addrOp:
			cout << "@\t";
			break;
		case multiplyOp:
			cout <<"*\t";
			break;
		case divideOp:
			cout <<"/\t";
			break;
		case plusOp:
			cout << "+\t";
			break;
		case minusOp:
			cout << "-\t";
			break;
		case minusUniOp:
			cout << "-\t";
			break;
		case lessOp:
			cout << "<\t";
			break;
		case assignOp:
			cout << ":=\t";
			break;
		case gotoOp:
			cout << "goto\t";
			break;
		case grOp:
			cout << ">\t";
			break;
		case labelOp:
			cout << "label\t";
			break;
		case exitOp:
			cout << "Program exited";
			break;
		case storeOp:
			cout << "store\t";
			break;
		case loadOp:
			cout << "load\t";
			break;
		case orOp:
			cout << "||\t";
			break;
		case andOp:
			cout << "&&\t";
			break;
		case notOp:
			cout << "!\t";
			break;
		case equalOp:
			cout << "==\t";
			break;
		case lessEqualOp:
			cout << "<=\t";
			break;
		case grEqualOp:
			cout << ">=\t";
			break;
		case inOp:
			cout << "in\t";
			break;
		case ssOp:
			cout << "ss\t";
			break;
		case intDivideOp:
			cout << "div\t";
			break;
		case modOp:
			cout << "mod\t";
			break;
		case asOp:
			cout << "as\t";
			break;
		case shiftLeftOp:
			cout << "<<\t";
			break;
		case shiftRightOp:
			cout << ">>\t";
			break;
		case notEqualOp:
			cout << "<>\t";
			break;
		case isOp:
			cout << "is\t";
			break;
		case xorOp:
			cout << "xor\t";
			break;
		case trueOp:
			cout << "true\t";
			break;
		case falseOp:
			cout << "false\t";
			break;
		case returnOp:
			cout << "return\t";  
			break;
		case callOp:
			cout << "call\t";
			break;
		case paramOp:
			cout << "param\t";
			break;  
		case dotLoadOp:
			cout << ".Load\t";
			break;  
		case dotStoreOp:
			cout << ".Store\t";
			break;
		case newOp:
			cout << "new\t";
			break;
		case disposeOp:
			cout << "dispose\t";
		case ifOp:
			cout << "ifOp\t";
			break;
		case enterOp:
			cout << "enter\t";
			break;
		case dotFunOp:
			cout << ".Fun\t";
			break;
		case enterMainOp:
			cout << "Entering Main\t";
			break;
	}
}
void print3AClist(){
	char sep[] = "*************************************\n";
	cout << sep << "Number of 3AC statements are:" << nextIndex << endl;
	for(int i=0;i<nextIndex;i++){
	   cout << i  << ":\t";
	   printReg(threeAClist[i].dest);
	   printOp(threeAClist[i].op);
	   printReg(threeAClist[i].src1);
	   printReg(threeAClist[i].src2);
	   // printf("\tisLabel:%d",threeAClist[i].isLabel);
	   cout << endl;
	}
	cout << sep;
}
