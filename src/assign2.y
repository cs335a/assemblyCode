%{
	#include "assign2.h"
	#include "assign2.tab.h"
	bool inFunction = false;
	int dummy;
	bool side;
	bool isWrite;
	#define left false
	#define right true
	int mainIndex;
	bool isNew = false;
	bool dotIsHere = false;
	int DEBUG = 0;
	int DEBUG_ASM = 0;
	int tempInt;
	char delem[] = "@";
	bool gotError = false;
	int lineNo;
	int yylex(void);
	void yyerror(char *);
	extern FILE *yyin;
	ThreeAC_src dest,src1,src2;
	#define threeAClistLen 10000
	threeAC threeAClist[threeAClistLen];
	argumentList* actualArgumentList;
	paramList* actualParamList;
	symTable* newSymbolTable;
	tupleList* objectTupleList;
	symTable* mainTable;
	unsigned nextIndex = 0;
	unsigned labelCounter = 0;
	unsigned tempCounter = 1;
	unsigned typeCounter = 1;
	unsigned enumCounter = 1;
	// tempVarState *tempVarArray = new tempVarState [tempCounter+1];
	threeAC ifMarkerVar;
	int arrayDimension = 0;
	arrayRangeList::reverse_iterator arrayRit;
	typeName charType ,integerType,shortIntType ,int64Type ,unsignedType,unsignedLongType ,floatType ,doubleType,booleanType,stringpType ,voidType,nilType,rangePType,objectPType,constructorType,destructorType;
	stack<symTable*> symStack;
	stack<loopStackElem> loopStack;
	#define currSymTable (symStack.top())
	char tab = '\t';
	ThreeAC_src nullThreeAC;
	ThreeAC_src tempThreeAC;
	list<char*>* identifierNameList = new list<char*>;
	typeName typeError  = {0,NULL,-1,0,0,NULL};
	typeName currEnumType = typeError;
	
	#include "semantics_utility.cc"
	
%}

%token  <structVal> COPERATORS  DEFINEC  DEFINE  ELSEC  ELSEIF ELIFC  ENDC  ENDIF  ERRORC  ERROR  FATAL  HPLUS  HMINUS  LONGSTRINGS  IFNDEF  IFDEF  IFOPT  IFC  IF  INCLUDEPATH  INFO  INLINE  IPLUS  IMINUS  I  LINKLIB  LINK  L  MACRO  MESSAGE  STATIC  STOP  UNDEFC  UNDEF FOR WARNINGS  WARNING  WARN  OBJECTPATH LEFTCURLY  RIGHTCURLY  LEFTBSTAR  RIGHTBSTAR COMMENT END_OF_PROGRAM  CHAR  ABSOLUTE  ARRAY  ASM  BEGINP  CASE  CONST  CONSTRUCTOR  DESTRUCTOR  DO  DOWNTO  END  FILEP   FUNCTION  GOTO  IMPLEMENTATION  INHERITED  INTERFACE  LABEL  NIL  OBJECT  OF  OPERATOR PROCEDURE  PROGRAM  RECORD  REINTRODUCE  REPEAT  SELF  SET STRING  TO  TYPE  UNIT  UNTIL  USES  VAR  WHILE  WITH  DISPOSE  FALSE  TRUE  EXIT  NEW  AS  CLASS  DISPINTERFACE  EXCEPT  EXPORTS  FINALIZATION  FINALLY  INTIALIZATION  IS  LIBRARY  ON  OUT  BITPACKED  PROPERLY  RAISE  RESOURCESTRING  THREADVAR  TRY  ABSTRACT  ALIAS  ASSEMBLER  PACKED  BREAK  CDECL  CONTINUE  CPPDECL  CVAR  DEFAULT  DEPRECATED  DYNAMIC  ENUMERATOR  EXPERIMENTAL  EXPORT  EXTERNAL  FAR  FAR16  FORWARD  GENERIC  HELPER  IMPLEMENTS  INDEX  INTERRUPT  IOCHECKS  LOCAL  NAME  NEAR  NODEFAULT  NORETURN  NOSTACKFRAME  OLDFPCCALL  OTHERWISE  OVERLOAD  OVERRIDE  PASCAL  PLATFORM  PRIVATE  PROTECTED  PUBLIC  PUBLISHED  READ  REGISTER  RESULT  SAFECALL  SAVEREGISTERS  SOFTFLOAT  SPECIALIZE  STDCALL  STORED  STRICT  UNALLIGNED  UNIMPLEMENTED  VARARGS  VIRTUAL  WRITE  INTEGER  CARDINAL  SHORTINT  SMALLINT  LONGINT  INT64  BYTE  LONGWORD  WORD  EXTENDED  BOOLEAN  REAL NESTED NEWLINE  SPACE BADIDENTIFIER  ALIEN THEN ELSE WRITEfloat

//////////////%left
%left EQUAL LG LESS GREATER LE GE IN
%left PLUS MINUS OR XOR
%left DIVIDE MUL DIV MOD AND
%left NOT

////////////
%right THEN ELSE
%token	CE PL  ML  PG  MG  SL  DL  SG  DG COLON  SEMICOLON  COMMA  LPAREN  RPAREN  LBRACKET  RBRACKET  CAP  DOTDOT  DOT  AT LL  GG  SHL  SHR INCLUDE SS

%token <structVal> HEXADECIMAL OCTAL BINARY INT EXPONENTIAL FLOAT STR CONST_CHAR IDENTIFIER

%union {
	nonTerminalAttr structVal;
};

%type <structVal> file cmdArguments identifierList initList varBody varDeclarations typeBody typeDeclarations constBody constDeclarations integer sign real number string constDeclaration typeDeclaration type simpleType ordinalType ordinalTypeWithoutIdentifier enumType enumList enumStmt range realType stringType limit structuredType arrayType rangeList rangeWithIdentfier recordType fieldList fixedFields pointerType  varDeclaration expr unsignedConstant functionCall actualParameterList exprList valueTypecast addressFactor statement ifStatement assignmtStatement procStatement allocParameterList compoundStatement statementList caseStatement caseList caseStmt caseCondList caseCond otherwisePart forInStatement forToDowntoStatement controlVariable initialValue finalValue  enumerable stringList repeatStatement whileStatement withStatement varReferenceList subroutineBlock funcDeclaration functionHeader parameterList parameterDeclarationList parameterDeclaration valueParameter parameterType  varReference nonPointerReference pointerReference indexedReference arrayReference  operatorDeclaration assignmtDefinition arithmeticDefinition comparisonDefinition objectType componentList component fieldOrMethodDefinitionList fieldOrMethodDefinition fieldDefinition constructorDeclaration destructorDeclaration constructorHeader destructorHeader methodDefinition methodDefinitionHeader visibilitySpecifier compOp addOp identifier factor op1 mulOp2 op2 simpleExpr term exprListArray funcMarker functionCallPart1 constructorDeclarationMarker destructorDeclarationMarker objFuncMarker  compExpr ifMarker whileMarker parameterTypeEnd


%start file

%%
file:
	PROGRAM {currEnumType.numBytes = 4;} identifier cmdArguments SEMICOLON initList BEGINP { emit(nullThreeAC,nullThreeAC,nullThreeAC,enterMainOp); mainIndex = nextIndex; threeAClist[nextIndex].isLabel = true; }  statementList END_OF_PROGRAM {emit(nullThreeAC,nullThreeAC,nullThreeAC,exitOp); 
	}
	;
cmdArguments:
	LPAREN identifierList RPAREN {

	}
	|
	/* epsilon*/	{

	}
	;
identifierList:    //cannot be empty
	identifier COMMA identifierList  {identifierNameList->push_front($1.name); }
	|
	identifier      { identifierNameList->push_front($1.name);}
	;
initList:
	varBody	initList {

	}
	|
	typeBody initList 	{

	}
	|
	constBody initList 	{

	}
	|
	funcDeclaration initList 	{

	}
	|
	operatorDeclaration initList 	{

	}
	|
	constructorDeclaration initList 	{

	}
	|
	destructorDeclaration initList 	{

	}
	|
	/* epsilon */  {

	}
	;
varBody:
	VAR varDeclarations		{

	}
	;
varDeclarations:
	varDeclaration varDeclarations 	{

	}
	|
	varDeclaration 		{

	}
	;
typeBody:
	TYPE typeDeclarations 	{

	}
	;
typeDeclarations:
	typeDeclaration typeDeclarations 	{

	}
	|
	typeDeclaration 	{

	}
	;
constBody:
	CONST constDeclarations 	{

	}
	;
constDeclarations:
	constDeclaration constDeclarations 	{

	}
	|
	constDeclaration 	{

	}
	;
integer:
	INT 	{

	}
	|
	OCTAL 	{

	}
	|
	HEXADECIMAL 	{

	}
	|
	BINARY 		{

	}
	;
sign:
	PLUS 	{ $$.code.op = plusOp;}
	|
	MINUS 	{ $$.code.op = minusOp;}
	;
real:
	FLOAT 	{

	}
	|
	EXPONENTIAL 	{

	}
	;
number:
	integer 	{ $$ = $1; }
	|
	real 	{ $$ = $1; }
	;
string:
	CONST_CHAR 		{
		$$.type = charType;
	}
	|
	STR 	{
		$$.type = stringpType;
		$$.type.numBytes = strlen($$.code.dest.strSrc);
	}
	;
constDeclaration:
	identifier EQUAL expr SEMICOLON 	{   
		if($3.isConstant){
			$1.code.dest.tupleSrc = insertTuple($3.type,$1.name);
			$1.code.dest.multiplexer = ACtuple;
			emit($1.code.dest,$3.code.dest,nullThreeAC,assignOp);
			$1.isConstant = true;
			$1.code.dest.tupleSrc->isConstant = true;
			$1.type = $3.type;
		}
		else{
			throwError(notConstant,$1.name);
			$$.type = typeError;
		}
	}
	 /*check for strings in expr*/
	;
typeDeclaration:
identifier EQUAL type SEMICOLON 	
	{ 
		if($3.type.id != typeError.id){
			if(!$3.type.isObject)
				$3.type.childTable = NULL;
			else{
				$3.type.numBytes = currSymTable->offset;
				$3.type.childTable = currSymTable;
				if(DEBUG){
					fprintf(stderr, "Popping stack at end of object Declaration and printing popped symbol Table\n" );
					printSymTable(currSymTable);
				}
				symStack.pop();
			}
			$$.type = $3.type = insertType($1.name,$3.type);
		}
		else{
			throwError(typeMismatch,$1.name);
		}
		if(DEBUG) printType($$.type,"typeDeclaration");
	}
	|
	identifier EQUAL { currEnumType = $1.type = insertType($1.name,4,0);   enumCounter = 0;} enumType SEMICOLON   { $$.type = $4.type = $1.type; }
	;
type:  
	simpleType 		{ $$.type = $1.type; $$.type.isObject = false; }
	|
	stringType 		{ $$.type = $1.type; $$.type.isObject = false;}
	|
	structuredType 	{$$.type = $1.type; }
	|
	pointerType		{$$.type = $1.type; $$.type.isObject = false;}
	;
simpleType:
	ordinalType 	{$$.type = $1.type; }
	|
	realType 	{$$.type = $1.type; }
	|
	range 	{$$.type = $1.type; }
	;
ordinalType:
	CHAR 	{ $$.type = charType ;}
	|
	INTEGER 	{$$.type = integerType; }
	|
	CARDINAL 	{$$.type = unsignedType; }
	|
	SHORTINT 	{$$.type = shortIntType; }
	|
	LONGINT 	{$$.type = int64Type; }
	|
	INT64 		{ $$.type = int64Type;}
	|
	BYTE 		{ $$.type = charType;}
	|
	WORD 		{ $$.type = unsignedType;}
	|
	LONGWORD 	{ $$.type = unsignedLongType;}
	|
	BOOLEAN 	{$$.type = booleanType; }
	|
	identifier 	{ $$.type = findType($1.name);}
	;
ordinalTypeWithoutIdentifier:
	CHAR 	{ $$.type = charType; }
	|
	INTEGER 	{$$.type = integerType; }
	|
	CARDINAL 	{$$.type = unsignedType; }
	|
	SHORTINT 	{$$.type = shortIntType; }
	|
	LONGINT 	{$$.type = int64Type; }
	|
	INT64 		{ $$.type = int64Type;}
	|
	BYTE 		{$$.type =  charType;}
	|
	WORD 		{ $$.type = unsignedType;}
	|
	LONGWORD 	{ $$.type = unsignedLongType;}
	|
	BOOLEAN 	{$$.type = booleanType; }
	;
enumType:
	LPAREN enumList RPAREN	{ $$.type = $2.type;}
	;
enumList:
	enumStmt COMMA enumList 	{  $$.type = $1.type;}
	|
	enumStmt 	{ $$.type  = $1.type; }
	;
enumStmt:
	identifier 	
	{   
		$$.type = currEnumType;
		$1.code.dest.tupleSrc = insertTuple(currEnumType,$1.name);
		$1.code.dest.multiplexer = ACtuple;
		tempThreeAC.multiplexer = ACint;
		tempThreeAC.intSrc = enumCounter++;
		emit($1.code.dest,tempThreeAC,nullThreeAC,assignOp);
	} 	
	|
	identifier CE integer   
	{
		$$.type = currEnumType;
		$1.code.dest.tupleSrc = insertTuple(currEnumType,$1.name);
		$1.code.dest.multiplexer = ACtuple;
		if($3.code.dest.intSrc < enumCounter){
			throwError(enumIndexOverflow,$1.name);
			$$.type = typeError;
		}
		tempThreeAC.intSrc = $3.code.dest.intSrc;
		tempThreeAC.multiplexer = ACint;
		enumCounter = $3.code.dest.intSrc + 1;
		emit($1.code.dest,tempThreeAC,nullThreeAC,assignOp);
	} 	
	; 
range:
	integer DOTDOT integer 	{ 
		$$.type = rangePType;
		$$.type.rangeList = new arrayRangeList;
		if( $3.code.dest.intSrc  < 0 ||  $1.code.dest.intSrc < 0 || ($3.code.dest.intSrc < $1.code.dest.intSrc) ){
			throwError(rangeUnderflow);
			$$.type = typeError;
		}
		else{
			if(DEBUG){
				fprintf(stderr, "in range:\tstart:%d\tend:%d\tpointer:0x%x\n",$1.code.dest.intSrc,$3.code.dest.intSrc,$$.type.rangeList );
			}
			$$.type.rangeList->push_front({$1.code.dest.intSrc,$3.code.dest.intSrc - $1.code.dest.intSrc+1});		
			$$.type.length = $3.code.dest.intSrc - $1.code.dest.intSrc;
			$$.type.hasRangeList = true;
			if(DEBUG){
				fprintf(stderr, "Exiting range:\n" );
				printType($$.type,"range");
			}
		}
	}
	; 
realType:
	EXTENDED 	{$$.type = findType("double"); }
	|
	REAL 		{$$.type = findType("float"); }
	;
stringType:
	STRING limit 	{ $$.type = stringpType; $$.type.length = $2.code.dest.intSrc;}
	;
limit:
	/*epsilon*/ 	{ $$.code.dest.intSrc = 256 /*defualt size*/ ; $$.code.dest.multiplexer = ACint; }
	|
	LBRACKET integer RBRACKET 	{ $$ = $2; } /*added by me it is LBRACKET integer RBRACKET in ref.pdf*/
	;
structuredType:
	arrayType 	{ 
			if($1.type.id != typeError.id && $1.type.hasRangeList)
				calcLengthAndLevel($1.type.rangeList,&($1.type.level),&($1.type.length));
			$$ = $1; 
			if(DEBUG) printType($$.type,"structuredType");
			$$.type.isObject = false;
		}
	|
	recordType 	{ $$ = $1; $$.type.isObject = true; }
	|
	objectType 	
	{
		$$ = $1;
		if($1.type.id != typeError.id){
			$$.type = objectPType;
			$$.type.isObject = true;
		}
		else{
			$$.type = typeError;
		}
	}
	;
arrayType:
	ARRAY LBRACKET rangeList RBRACKET OF type 	
	{   
		if(DEBUG){
			printType($3.type,"arrayType-rangeList");
			printType($6.type,"arrayType-type");
		}
		if($3.type.id != typeError.id && $6.type.id != typeError.id){
			$$.type = $6.type;
			if($6.type.hasRangeList){
				for(auto i = $6.type.rangeList->begin(); i != $6.type.rangeList->end(); i++){
					$3.type.rangeList->push_back(*i);
				}
			}
			$$.type.rangeList = $3.type.rangeList;
			$$.type.hasRangeList = true;
			if(DEBUG)
				printType($$.type,"arrayType");
		}
		else
			$$.type = typeError;
	} /* Static Array*/
	|
	ARRAY OF type 	{ $$.type = $3.type; if($$.type.id != typeError.id ) {$$.type.level++;$$.type.hasRangeList = false; } }  /*Dynamic Array*/
	;
rangeList:
	rangeWithIdentfier COMMA rangeList 	
	{ 	
		if($1.type.id == rangePType.id && $3.type.id == rangePType.id){
			for(auto i = $1.type.rangeList->begin(); i != $1.type.rangeList->end(); i++){
				$3.type.rangeList->push_front(*i);
			}
			$$.type = $3.type;
		}
		else{
			throwError(typeMismatch,$1.name);
			$$.type = typeError;
		}
		if(DEBUG) {printType($1.type,"rangeList");}
	}
	|
	rangeWithIdentfier 	{ $$ = $1; /*if($1.type.id != typeError.id){ $$.type.level++;}*/ if(DEBUG) {printType($1.type,"rangeList-Identifier");} }
	;
rangeWithIdentfier:
	range 	{ $$ = $1;}
	|
	identifier 	{ if($1.type.id != rangePType.id ) {throwError(typeMismatch,$1.name); $$.type = typeError;}  $$ = $1;}
	;
recordType:
	RECORD END 	{

	}
	|
	RECORD fieldList END 	{

	}
	;
fieldList:
	fixedFields 	{

	}
	;
fixedFields:
	identifierList COLON type SEMICOLON fixedFields 	{

	}
	|
	identifierList COLON type SEMICOLON 	{

	}
	;
pointerType:
	CAP type 	{ 
		$$.type = $2.type;
		$$.type.level++;
		$$.type.numBytes = 4;
	}
	;
/*proceduralType:
	procOrFuncTypeHeader 	{

	}
	;
procOrFuncTypeHeader:
	functionTypeHeader 	{

	}
	|
	procedureTypeHeader 	{

	}
	;
functionTypeHeader:
	FUNCTION parameterList COLON type 	{

	}
	;
procedureTypeHeader:
	PROCEDURE parameterList 	{

	}
	;*/
varDeclaration:
	identifierList COLON type SEMICOLON	{ 
		if(!$3.type.isObject && $3.type.id != typeError.id){
			for(auto i = identifierNameList->begin(); i != identifierNameList->end() ; i++){
				tupleNew* tempTuple = insertTuple($3.type,*i);
				tempTuple->isConstant = false;
			}
		}
		else if(!$3.type.isObject){
			throwError(structuredTypeInDeclaration,"In varDeclaration");
		}
		delete identifierNameList;
		identifierNameList = new list<char*>;
	} 
	;
compExpr:
	expr	{
		$$.code.dest.intSrc = newTmp();
		$$.code.dest.multiplexer = ACtemp;
		$$.code.dest.type = $1.type;
		ifMarkerVar = $$.code;
		$1.trueList = backpatch(nextIndex, $1.trueList);
		emit($$.code.dest, nullThreeAC, nullThreeAC, trueOp);
		emit({NULL, nextIndex+2, NULL, NULL, NULL, NULL, AClabel}, nullThreeAC,nullThreeAC, gotoOp);
		$1.falseList = backpatch(nextIndex, $1.falseList);
		emit($$.code.dest, nullThreeAC, nullThreeAC, falseOp);
		$$.trueList = new listType;
		$$.falseList = new listType;
		mergeList($$.trueList, $1.trueList);
		mergeList($$.falseList, $1.falseList);
	}
	;
expr:
	simpleExpr {
		if($1.type.id == booleanType.id){
			$$.type = booleanType;
			$$.trueList = new listType;
			$$.falseList = new listType;
			mergeList($$.falseList,$1.falseList); 
			mergeList($$.trueList, $1.trueList);
		}
		else{
			$$ = $1;
		}
	}
	|
	simpleExpr op1 simpleExpr { 
		$$.isConstant = ($1.isConstant & $3.isConstant);
		typeName newType = validType($1.type, $3.type, $2.code.op);
		if(newType.id != typeError.id){
			$$.isConstant = ($1.isConstant & $3.isConstant);
			$$.isConstantInt = ($1.isConstantInt & $3.isConstantInt);
			if($2.isComp){
				$$.type = booleanType;
				$$.trueList = new listType(1, nextIndex);
				$$.falseList = new listType(1, nextIndex+1);
				emit(nullThreeAC, $1.code.dest, $3.code.dest, $2.code.op);
				emit(nullThreeAC, nullThreeAC, nullThreeAC, gotoOp);
			}
			else{
				$$.type = newType;
				$$.code.dest.intSrc = newTmp();
				$$.code.dest.multiplexer = ACtemp;
				$$.code.dest.type = $$.type;
				emit($$.code.dest, $1.code.dest, $3.code.dest, $2.code.op);
			}
		}
		else{
			$$.type = $$.code.dest.type = typeError;
			throwError(typeMismatch);
		}
	}
	;
simpleExpr:
	term   {
		if($1.type.id == booleanType.id){
			$$.type = booleanType;
			$$.trueList = new listType;
			$$.falseList = new listType;
			mergeList($$.falseList,$1.falseList);
			mergeList($$.trueList, $1.trueList);
		}
		else{
			$$ = $1;
		}
	}
	|
	term addOp {
		if($1.type.id == booleanType.id && $2.isComp && $2.isAndOr){
			if($2.code.op == andOp){
				$1.trueList = backpatch(nextIndex, $1.trueList); 
			}
			else if($2.code.op == orOp){
				$1.falseList = backpatch(nextIndex, $1.falseList);
			}
		}
	} simpleExpr   { 
		$$.isConstant = ($1.isConstant & $4.isConstant);
		typeName newType = validType($1.type, $4.type, $2.code.op);
		if(newType.id != typeError.id){
			$$.isConstant = ($1.isConstant & $4.isConstant);
			$$.isConstantInt = ($1.isConstantInt & $4.isConstantInt);
			if($2.isComp){
				$$.type = booleanType;
				if($2.isAndOr){
					$$.falseList = new listType;
					$$.trueList = new listType;
					mergeList($4.falseList,$1.falseList);
					mergeList($$.falseList,$4.falseList);
					mergeList($4.trueList,$1.trueList);
					mergeList($$.trueList,$4.trueList);
				}
			}
			else{
				$$.type = newType;
				$$.code.dest.intSrc = newTmp();
				$$.code.dest.multiplexer = ACtemp;
				$$.code.dest.type = newType;
				emit($$.code.dest, $1.code.dest, $4.code.dest, $2.code.op);
			}
		}
		else{
			$$.type = $$.code.dest.type = typeError;
			throwError(typeMismatch);
		} 
	}
	;
term:
	factor {
		if($$.type.id != typeError.id){
			if($1.type.id == booleanType.id){
				$$.type = booleanType;
				$$.trueList = new listType;
				$$.falseList = new listType;
				mergeList($$.falseList,$1.falseList);
				mergeList($$.trueList, $1.trueList);
			}
			else{
				$$ = $1;
			}
		}
		else{
			$$.type = typeError;
		}
	}
	|
	factor mulOp2 {
		if($1.type.id == booleanType.id && $2.isComp && $2.isAndOr){
			if($2.code.op == andOp){
				$1.trueList = backpatch(nextIndex, $1.trueList); 
			}
			else if($2.code.op == orOp){
				$1.falseList = backpatch(nextIndex, $1.falseList);
			}
		}
	}
	term  {
		typeName newType = validType($1.type, $4.type, $2.code.op);
		if(newType.id != typeError.id){
			$$.isConstant = ($1.isConstant & $4.isConstant);
			$$.isConstantInt = ($1.isConstantInt & $4.isConstantInt);
			if($2.isComp){
				if($2.isAndOr){
					$$.falseList = new listType;
					$$.trueList = new listType;
					mergeList($4.falseList,$1.falseList);
					mergeList($$.falseList,$4.falseList);
					mergeList($4.trueList,$1.trueList);
					mergeList($$.trueList,$4.trueList);
				}
				$$.type = booleanType;
			}
			else{
				$$.type = newType;
				$$.code.dest.intSrc = newTmp();
				$$.code.dest.multiplexer = ACtemp;
				$$.code.dest.type = newType;
				emit($$.code.dest, $1.code.dest, $4.code.dest, $2.code.op);
			}
		}
		else{
			$$.type = $$.code.dest.type = typeError;
			throwError(typeMismatch);
		} 
	}
	;
factor:
	LPAREN expr RPAREN 	{ 
		$$=$2;
	}
	|
	functionCall 	{
		$$ = $1;
	}
	|
	varReference 	{ 
		$$=$1;
		if($1.type.id == booleanType.id){
			$$.type = booleanType;
			$$.trueList = new listType(1, nextIndex);
			$$.falseList = new listType(1, nextIndex+1);
			emit(nullThreeAC, $1.code.dest, nullThreeAC, ifOp);
			emit(nullThreeAC, nullThreeAC, nullThreeAC, gotoOp);
		}
	}
	|
	unsignedConstant   { $$ = $1;}
	|
	NOT factor 	{
		if($2.type.id==booleanType.id && $2.type.level == 0){
			$$ = $2;
			$$.type = booleanType;
			// $$.code.dest.intSrc = newTmp();
			// $$.code.dest.multiplexer = ACtemp;
			// $$.code.dest.type = $$.type;
			// emit($$.code.dest,$2.code.dest,nullThreeAC, notOp);
			$$.trueList = $2.falseList;
			$$.falseList = $2.trueList;
		}
		else{
			fprintf(stderr, "Cannot negate non-boolean value at %d.\n", lineNo);
		}
	}
	|
	sign factor  	{ 
		if($2.type.id != typeError.id &&  $2.type.id <= doubleType.id && $2.type.level == 0){
			if($1.code.op == minusOp){
				$$.type = $2.type;
				if($2.type.id == unsignedType.id)
					$$.type = integerType;
				else if($2.type.id == unsignedLongType.id)
					$$.type = int64Type;
				$$.code.dest.intSrc = newTmp();
				$$.code.dest.multiplexer = ACtemp;
				$$.code.dest.type = $$.type;
				emit($$.code.dest,$2.code.dest,nullThreeAC, minusUniOp);
			}
			else{
				$$ = $2;
			}
		}
		else{
			$$.type = $$.code.dest.type = typeError;
			throwError(undefinedType);
		}
	}
	|
	valueTypecast 	{
		$$ = $1;
	}
	|
	addressFactor 	{
		if($1.type.id!=typeError.id){
			$$.type = $1.type;
		}
		else{
			$$.type = typeError;
			throwError(typeMismatch);
		}
	}
	|
	TRUE 	{ $$.code.dest.boolSrc = true; $$.code.dest.multiplexer = ACbool; $$.type = $$.code.dest.type; booleanType;} /*true and false added by me for as:boolean = TRUE*/
	|
	FALSE 	{ $$.code.dest.boolSrc = false; $$.code.dest.multiplexer = ACbool; $$.type = $$.code.dest.type; booleanType;}
	;/*
	|
	setConstructor
	*/
unsignedConstant:
	number 	{ $$ = $1;}
	|
	string { $$ = $1;}
	|
	NIL 	{ $$.type = $$.code.dest.type = nilType; $$.code.dest.intSrc = 0; $$.code.dest.multiplexer = ACnil;}
	;
functionCall:
	functionCallPart1 actualParameterList 	{
		bool pushed = false;
		char* newName = concat($1.name,actualParamList);
		if($1.type.childTable != NULL){
			if(DEBUG) fprintf(stderr, "In functionCall -- Pushing childTable newName is:%s\n",newName );
			symStack.push($1.type.childTable);
			pushed = true;
		}
		if(DEBUG) fprintf(stderr, "In functionCall --  Pushed childTable\n" );
		if(!dotIsHere && !exists(newName)){
			if(DEBUG)
				fprintf(stderr, "IN -- functionCall, no matching function found for %s\n",newName );
			printSymTable(currSymTable);
			throwError(undefinedVar,newName);
			$$.type = $$.code.dest.type = typeError;
		}
		else if(dotIsHere && currSymTable->table.find(string(newName)) == currSymTable->table.end()){
			if(DEBUG)
				fprintf(stderr, "IN -- functionCall, no matching function found for %s\n",newName );
			printSymTable(currSymTable);
			throwError(undefinedVar,newName);
			$$.type = $$.code.dest.type = typeError;
		}
		else{
			tupleNew* tempTuple;
			if(dotIsHere)
				tempTuple = (currSymTable->table.find(string(newName)))->second;
			else
				tempTuple = findTuple(newName);
			bool mismatch = false;
			if(actualParamList != NULL && tempTuple->argList != NULL && tempTuple->argList->size() == actualParamList->size()){
				auto j = tempTuple->argList->begin();
				for(auto i = actualParamList->begin(); i != actualParamList->end(); i++,j++){
					if(i->type.id != j->type.id)
						mismatch = true;
				}
			}
			else if(actualParamList == NULL && tempTuple->argList == NULL)
				mismatch = false;
			else
				mismatch = true;
			if(mismatch){
				$$.type = $$.code.dest.type = typeError;
				if(DEBUG)
					fprintf(stderr, "Parameter Mismatch for function:%s \n",tempTuple->name.c_str() );
				throwError(undefinedType,newName);
			}
			else{
				if(dotIsHere){
					tempThreeAC.tupleSrc = tempTuple;
					tempThreeAC.multiplexer = ACtuple;
					emit(nullThreeAC,$1.code.dest,tempThreeAC,dotFunOp);
				}
				if(actualParamList != NULL)
				{
					for(auto i = actualParamList->rbegin(); i != actualParamList->rend();i++)
						emit(*i,nullThreeAC,nullThreeAC,paramOp);
				}
				$$.code.dest.intSrc = newTmp();
				$$.code.dest.multiplexer = ACtemp;
				$$.type = $$.code.dest.type = tempTuple->type;
				tempThreeAC.intSrc = tempTuple->startIndex;
				tempThreeAC.multiplexer = AClabel;
				emit($$.code.dest,tempThreeAC,nullThreeAC,callOp);
			}
		}
		if(actualParamList != NULL)
		   delete actualParamList;
		if(pushed)
			symStack.pop();
		dotIsHere = false;
	}
	;
actualParameterList:
	LPAREN {actualParamList = NULL;} RPAREN  	{

	}
	|
	LPAREN { actualParamList = new paramList;} exprList RPAREN {

	}
	;
exprList:
	expr COMMA exprList    
	{
		if($1.type.id != typeError.id && $3.type.id != typeError.id)
			actualParamList->push_front($1.code.dest);
		else
			$$.type = $$.code.dest.type = typeError;
	}
	|
	expr
	{
		if($1.type.id != typeError.id )
			actualParamList->push_front($1.code.dest);
		else
			$$.type = $$.code.dest.type = typeError;
	} 
	;
functionCallPart1:
	pointerReference foundDOT DOT identifier 	 {
		if(exists($1.name) && $1.type.level == 0){
			if($1.type.childTable != NULL){
				$$.name = $4.name;
				$$.type = $1.type;
				$$.code.dest = $1.code.dest;
				if(DEBUG) fprintf(stderr, "\"%s\" exists with type:%s\n",$1.name,$1.type.name );
			}
			else{
				if(DEBUG) fprintf(stderr, "in nonPointerReference-2 childTable for:%s with type:%s is NULL\n",$1.name,$1.type.name );
				$$.type = typeError;
				throwError(notObject,$1.name);
			}
		}
		else{
			if(DEBUG) fprintf(stderr, "in nonPointerReference-2 type.id:%d type.name:%s type.level:%d\n",$1.type.id,$1.type.name,$1.type.level );
			$$.type = typeError;
			throwError(undefinedVar,$1.name);
		}
	}
	|
	identifier 	{
		$$ = $1;
		$$.type.childTable = currSymTable;
	}
	;
foundDOT:
	/*epsilon*/ { dotIsHere = true;}
	;
valueTypecast:
	ordinalTypeWithoutIdentifier LPAREN expr RPAREN 	{
		$$ = $3;
		if(($3.type.id != typeError.id) && ($1.type.id != typeError.id)){
			if($1.type.id >= $3.type.id && $1.type.id <= doubleType.id){
				$$.type = $$.code.dest.type = $1.type;
			}
			else{
				$$.type = $$.code.dest.type = typeError;
				throwError(typeMismatch);    
			}
		}
		else{
			$$.type = $$.code.dest.type = typeError;
			throwError(typeMismatch);
		}
	} /*Identifier taken care in functionCall*/	;
addressFactor:
	AT varReference 	{
		if($2.type.id!=typeError.id){
			$$.type = $2.type;
			$$.type.level++;
			$$.code.dest.intSrc = newTmp();
			$$.code.dest.multiplexer = ACtemp;
			$$.code.dest.type = $$.type;
			emit($$.code.dest, $2.code.dest, nullThreeAC, addrOp);
		//    fprintf(stderr, "Identifier:%s has level:%d\n",$2.name,$2.code.dest.tupleSrc->type.level );
		}
		else{
			$$.type = $$.code.dest.type = typeError;
			throwError(typeMismatch);
		}
	} /*identifier can be procedure | function | qualified method */
	;
statement:
	compoundStatement 	{ 
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	caseStatement 	{
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	forToDowntoStatement{
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	forInStatement{
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	repeatStatement 	{
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	whileStatement 	{
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	assignmtStatement 	{
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	procStatement 	{ 
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	ifStatement 	{
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	withStatement{
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	EXIT 	{
		$$.type = voidType;
		emit(nullThreeAC,nullThreeAC,nullThreeAC,exitOp);
		print3AClist();
		return 0;
	}
	|
	BREAK	{
		if(!loopStack.empty()){
			loopStack.top().breakList->push_front(nextIndex);
			emit(nullThreeAC, nullThreeAC, nullThreeAC, gotoOp);
			$$.type = voidType;
		}
		else{
			$$.type = typeError;
		}
	}
	|
	CONTINUE	{
		if(!loopStack.empty()){
			loopStack.top().continueList->push_front(nextIndex);
			emit(nullThreeAC, nullThreeAC, nullThreeAC, gotoOp);
			$$.type = voidType;
		}
		else{
			$$.type = typeError;
		}
	}
	;
ifStatement:
	IF compExpr ifMarker THEN  statement 	{
		if($2.type.id == booleanType.id && $5.type.id != typeError.id){
			$3.falseList = backpatch(nextIndex, $3.falseList);
			$$.type = voidType;
		}
		else{
			$$.type = typeError;
		}
	}
	|
	IF compExpr ifMarker THEN statement ELSE { 
		emit(nullThreeAC, nullThreeAC, nullThreeAC, gotoOp);
		$2.index = nextIndex-1;
		$3.falseList = backpatch(nextIndex, $3.falseList);
	} statement 	{
		if($2.type.id == booleanType.id && $5.type.id!=typeError.id && $8.type.id!=typeError.id){
			auto temp2 = new listType(1, $2.index);
			backpatch(nextIndex, temp2);
			$$.type = voidType;
		}
		else{
			$$.type = typeError;
		}
	}
	;
ifMarker:
	/*epsilon*/{
		emit( {NULL, nextIndex+2, NULL, '\0', 0, false, AClabel, integerType},ifMarkerVar.dest, nullThreeAC, ifOp);
		$$.falseList = new listType(1, nextIndex);
		emit(nullThreeAC, nullThreeAC, nullThreeAC, gotoOp);
	}
	;
assignmtStatement:
	varReference CE 
	{
		if($1.type.id != typeError.id && $1.isPointer && threeAClist[nextIndex-1].op == loadOp ){
			nextIndex--;
			$1.code.dest = threeAClist[nextIndex].src1;
		}
		else if($1.type.id != typeError.id && threeAClist[nextIndex-1].op == dotLoadOp){
			threeAClist[nextIndex-1].op = dotStoreOp;
			$1.isPointer = true;
		}
	} 
	expr	{
		if($1.code.dest.multiplexer == ACtuple && $1.code.dest.tupleSrc->isConstant == 1){
			$$.type = typeError;
			throwError(constantAssignment,$1.name);
		}
		else if(validType($1.type, $4.type, assignOp).id != typeError.id)
		{
			$$.type = voidType;
			if($1.isPointer){
				emit($1.code.dest, $4.code.dest,nullThreeAC, storeOp);
			}
			else{
				emit($1.code.dest, $4.code.dest, nullThreeAC, assignOp);
			}
		}
		else{
			$$.type = typeError;
			fprintf(stderr, "destId:%d\tsrcId:%d\tdestLevel:%d\tsrcLEvel:%d\tname:%s\n",$1.type.id,$4.type.id,$1.type.level,$4.type.level,$1.name );
			throwError(typeMismatch,"assignmtStatement");
		}
	}
	;
procStatement:
	functionCall 	{
		
	}
	|
	READ actualParameterList 	{
		if(actualParamList != NULL){
			for(auto i = actualParamList->rbegin(); i != actualParamList->rend(); i++){ 
				emit(*i,nullThreeAC,nullThreeAC,paramOp);
			}
			tempThreeAC.multiplexer = ACread;
			ThreeAC_src tempThreeAC2;
			tempThreeAC2.intSrc = newTmp();
			tempThreeAC2.type = integerType;
			tempThreeAC2.multiplexer = ACtemp;
			emit(tempThreeAC2,tempThreeAC,nullThreeAC,callOp);
		}
	}
	|
	WRITE actualParameterList 	{
		if(actualParamList != NULL){
			tempThreeAC.multiplexer = ACwrite;
			for(auto i = actualParamList->rbegin(); i != actualParamList->rend(); i++){ 
				emit(*i,tempThreeAC,nullThreeAC,paramOp);
			}
			ThreeAC_src tempThreeAC2;
			tempThreeAC2.intSrc = newTmp();
			tempThreeAC2.type = integerType;
			tempThreeAC2.multiplexer = ACtemp;
			emit(tempThreeAC2,tempThreeAC,nullThreeAC,callOp);
		}
	}
	|
	NEW {actualParamList = new paramList; isNew = true;} allocParameterList 	{

	}
	|
	DISPOSE {actualParamList = new paramList; isNew = false;} allocParameterList 	{

	}
	;
allocParameterList:
	LPAREN identifier/*object Identifier*/ DOT identifier /*constructor name*/ LPAREN exprList RPAREN RPAREN 
	{
		tupleNew* tempTuple;
		if(exists($2.name) && $2.type.level > 0){
			if($2.type.childTable != NULL){
				symStack.push($2.type.childTable);
				char* newName = concat($4.name,actualParamList);
				auto tempTupleIt = $2.type.childTable->table.find(string(newName));
				if(tempTupleIt != $2.type.childTable->table.end()){
					tempTuple = tempTupleIt->second;
					typeName tempType;
					opType tempOp;
					if(isNew){
						tempType = constructorType;
						tempOp = newOp;
					}
					else{
						tempType = destructorType;
						tempOp = disposeOp;
					}
					if(tempTuple->type.id == tempType.id){
			bool mismatch = false;
			if(actualParamList != NULL && tempTuple->argList != NULL && tempTuple->argList->size() == actualParamList->size()){
				auto j = tempTuple->argList->begin();
				for(auto i = actualParamList->begin(); i != actualParamList->end(); i++,j++){
					if(i->type.id != j->type.id)
						mismatch = true;
				}
			}
			else if(actualParamList == NULL && tempTuple->argList == NULL)
				mismatch = false;
			else
				mismatch = true;
					if(mismatch){
						$$.type = $$.code.dest.type = typeError;
						if(DEBUG)
							fprintf(stderr, "Parameter Mismatch for function:%s \n",tempTuple->name.c_str() );
						throwError(undefinedType,newName);
					}
					else{
						if(actualParamList != NULL)
						{
							for(auto i = actualParamList->begin(); i != actualParamList->end();i++)
						emit(*i,nullThreeAC,nullThreeAC,paramOp);
						}   
						$$.code.dest.intSrc = newTmp();
						$$.code.dest.multiplexer = ACtemp;
						$$.type = $$.code.dest.type = tempTuple->type;
						tempThreeAC.intSrc = tempTuple->startIndex;
						tempThreeAC.multiplexer = ACint;
						ThreeAC_src tempThreeAC2;
						tempThreeAC2.tupleSrc = tempTuple;
						tempThreeAC2.multiplexer = ACtuple;
						emit($2.code.dest,tempThreeAC,tempThreeAC2,tempOp);
						if(DEBUG) fprintf(stderr, "in NEW-2 \n");
					}
				}
				else{
				   if(DEBUG) fprintf(stderr, "in NEW-2 -- \"%s\" is not a constructor for:%s is\n",$4.name,$2.type.name );
					printSymTable($2.type.childTable);
					$$.type = typeError;
					throwError(undefinedVar,$2.type.name);
				}
			}
			else{
				if(DEBUG) fprintf(stderr, "in NEW-2 childTable for:%s with type:%s is NULL\n",$2.name,$2.type.name );
				$$.type = typeError;
				throwError(notObject,$2.name);
			}
			symStack.pop();
		}
		else{
			if(DEBUG) fprintf(stderr, "in NEW-2 type.id:%d type.name:%s type.level:%d\n",$2.type.id,$2.type.name,$2.type.level );
			$$.type = typeError;
			throwError(undefinedVar,$2.name);
		}
	   }
	   if(actualParamList != NULL)
		   delete actualParamList;
	}
	;
compoundStatement:
	BEGINP statementList END 	{

	}
	;
statementList:
	statement SEMICOLON statementList  { 
		if($1.type.id == voidType.id && $3.type.id == voidType.id){
			$$.type = $1.type;
		}
		else{
			$$.type = typeError;
			throwError(typeMismatch);
		}
	}
	|
	/*epsilon*/    {
		$$.type = voidType;
	}
	;
caseStatement:
	CASE expr OF caseList END 	{

	}
	|
	CASE expr OF caseList otherwisePart statementList END 	{

	}
	;
caseList:
	caseStmt SEMICOLON caseList 	{

	}
	|
	caseStmt SEMICOLON 	{

	}
	;
caseStmt:
	caseCondList COLON statement 	{

	}
	;
caseCondList:
	caseCond COMMA caseCondList 	{

	}
	|
	caseCond 	{

	}
	;
caseCond:
	expr 	{

	}
	|
	expr DOTDOT expr 	{

	}
	;
otherwisePart:
	OTHERWISE COLON 	{

	}
	;
forToDowntoStatement:
	FOR controlVariable CE initialValue {
		emit($2.code.dest,$4.code.dest,nullThreeAC,assignOp);
	} TO finalValue whileMarker {
		emit({NULL, nextIndex+2, NULL, NULL, NULL, NULL, AClabel},$2.code.dest, $7.code.dest, lessEqualOp);
		$2.index = nextIndex;
		emit(nullThreeAC, nullThreeAC, nullThreeAC, gotoOp);
	}DO statement {
		if($2.type.id!=typeError.id && $4.type.id!=typeError.id && $7.type.id!=typeError.id && $11.type.id!=typeError.id){
			emit($2.code.dest, $2.code.dest, {NULL, 1, NULL, NULL, NULL, NULL, ACint}, plusOp);
			emit({NULL, $2.index-1, NULL, NULL, NULL, NULL, AClabel}, nullThreeAC,nullThreeAC, gotoOp);
			listType* temp3 = new listType(1, $2.index);
			backpatch(nextIndex, temp3);
			if(!loopStack.top().breakList->empty()){
				backpatch(nextIndex, loopStack.top().breakList);
			}
			if(!loopStack.top().continueList->empty()){
				backpatch($8.code.dest.intSrc, loopStack.top().continueList);
			}
			loopStack.pop();
			$$.type = voidType;
		}
		else{
			$$.type = typeError;
		}
	}
	|
	FOR controlVariable CE initialValue{
		emit($2.code.dest,$4.code.dest,nullThreeAC,assignOp);
	}  DOWNTO finalValue whileMarker {
		emit({NULL, nextIndex+2, NULL, NULL, NULL, NULL, AClabel},$2.code.dest, $7.code.dest, grEqualOp);
		$2.index = nextIndex;
		emit(nullThreeAC, nullThreeAC, nullThreeAC, gotoOp);
	} DO statement{
		if($2.type.id!=typeError.id && $4.type.id!=typeError.id && $7.type.id!=typeError.id && $11.type.id!=typeError.id){
			emit($2.code.dest, $2.code.dest, {NULL, 1, NULL, NULL, NULL, NULL, ACint}, minusOp);
			emit({NULL, $2.index-1, NULL, NULL, NULL, NULL, AClabel}, nullThreeAC,nullThreeAC, gotoOp);
			listType* temp3 = new listType(1, $2.index);
			backpatch(nextIndex, temp3);
			if(!loopStack.top().breakList->empty()){
				backpatch(nextIndex, loopStack.top().breakList);
			}
			if(!loopStack.top().continueList->empty()){
				backpatch($8.code.dest.intSrc, loopStack.top().continueList);
			}
			loopStack.pop();
			$$.type = voidType;
		}
		else{
			$$.type = typeError;
		}
	}
	;
forInStatement:
	FOR controlVariable IN enumerable DO statement
	;
controlVariable:
	identifier 	{
		if($1.type.id == integerType.id || $1.type.id == shortIntType.id || $1.type.id == int64Type.id || $1.type.id == unsignedType.id || $1.type.id == unsignedLongType.id){
			$$ = $1;
		}
		else{
			$$.type = typeError;
		}
	} /*variable identifier*/
	;
initialValue:
	expr 	{
		if($1.type.id == integerType.id || $1.type.id == shortIntType.id || $1.type.id == int64Type.id || $1.type.id == unsignedType.id || $1.type.id == unsignedLongType.id){
			$$ = $1;
		}
		else{
			$$.type = typeError;
		}
	}
	;
finalValue:
	expr 	{
		if($1.type.id == integerType.id || $1.type.id == shortIntType.id || $1.type.id == int64Type.id || $1.type.id == unsignedType.id || $1.type.id == unsignedLongType.id){
			$$ = $1;
		}
		else{
			$$.type = typeError;
		}
	}
	;
enumerable:
	enumType 	{

	}
	|
	string 	{

	}
	|
	LPAREN stringList RPAREN 	{

	}
	;
stringList:
	string COMMA stringList 	{

	}
	|
	string 	{

	}
	;
repeatStatement:
	REPEAT whileMarker statementList UNTIL compExpr{
		emit($2.code.dest,ifMarkerVar.dest, nullThreeAC, ifOp);
		if(!loopStack.top().breakList->empty()){
			backpatch(nextIndex, loopStack.top().breakList);
		}
		if(!loopStack.top().continueList->empty()){
			backpatch($2.code.dest.intSrc, loopStack.top().continueList);
		}
		loopStack.pop();
	}
	;
whileStatement:
	WHILE whileMarker compExpr {
		emit( {NULL, nextIndex+2, NULL, NULL, NULL, NULL, AClabel},ifMarkerVar.dest, nullThreeAC, ifOp);
		$3.falseList = new listType(1, nextIndex);
		emit(nullThreeAC, nullThreeAC, nullThreeAC, gotoOp);
	} DO statement {
		emit($2.code.dest, nullThreeAC, nullThreeAC, gotoOp);
		$3.falseList = backpatch(nextIndex, $3.falseList);
		if(!loopStack.top().breakList->empty()){
			backpatch(nextIndex, loopStack.top().breakList);
		}
		if(!loopStack.top().continueList->empty()){
			backpatch($2.code.dest.intSrc, loopStack.top().continueList);
		}
		loopStack.pop();
		if($3.type.id == booleanType.id && $6.type.id!=typeError.id){
			$$.type = voidType;
		}
		else{
			$$.type = typeError;
		}
	}
	;
whileMarker:
	/*epsilon*/{
		loopStackElem* temp = new loopStackElem; 
		temp->breakList = new listType;
		temp->continueList = new listType;
		loopStack.push(*temp);
		$$.code.dest.intSrc = nextIndex;
		$$.code.dest.multiplexer = AClabel;
	}
	;
withStatement:
	varReferenceList DO statement
	;
varReferenceList:
	varReference COMMA varReferenceList 	{

	}
	|
	varReference 	{

	} 
	;
subroutineBlock:
	initList compoundStatement SEMICOLON 	{
		if(DEBUG){
			fprintf(stderr, "Popping stack and printing popped symbol Table\n" );
			printSymTable(currSymTable);
		}
		if(currSymTable->returnVar != NULL){
			tempThreeAC.tupleSrc = currSymTable->returnVar;
			tempThreeAC.multiplexer = ACtuple;
			emit(tempThreeAC,nullThreeAC,nullThreeAC,returnOp);
		}
		else{
			emit(nullThreeAC,nullThreeAC,nullThreeAC,returnOp);
		}
		symStack.pop();
		if(symStack.empty())
		{
			fprintf(stderr,"ERROR!! Symbol Table Stack is empty\n");
			exit(2);
		}

	}
	;
funcDeclaration:
	functionHeader SEMICOLON funcMarker
	{
		if($1.type.id != typeError.id){
			if(DEBUG){
				fprintf(stderr, "In funcDeclaration -- Inserting %s\n",$1.name );
			}
			$3.code.dest.tupleSrc = insertTuple($1.type,$1.name,actualArgumentList);
			$3.code.dest.multiplexer = ACtuple;
			$3.type = $1.type;
			emit($3.code.dest,nullThreeAC,nullThreeAC,enterOp);
		}
		else{
			fprintf(stderr, "ERROR in funcDeclaration typeId is:%d\n",$1.type.id );
			throwError(InvalidFunctionDefinition,$1.name);
		}
	}
	subroutineBlock 	{ $$ = $3 ;}
	|
	FUNCTION identifier DOT identifier parameterList COLON type objFuncMarker SEMICOLON
	{
		typeName tempType = findType($2.name);
		char* newName;
		if($5.type.id != typeError.id &&  $7.type.id != typeError.id && tempType.id != typeError.id && tempType.isObject && tempType.childTable != NULL  ){
			symStack.push(tempType.childTable);
			newName = concat($4.name,actualArgumentList);
			if(actualArgumentList != NULL){
				delete actualArgumentList;
				actualArgumentList = NULL;
			}
			tupleNew* tempTuple = findTuple(newName);
			symStack.pop();
			if(tempTuple != NULL && $7.type.id == tempTuple->type.id && tempTuple->childTable != NULL){
				symStack.push(tempTuple->childTable);
				tempTuple->startIndex = nextIndex;
				tempThreeAC.multiplexer = ACtuple;
				tempThreeAC.tupleSrc = tempTuple;
				emit(tempThreeAC,nullThreeAC,nullThreeAC,enterOp);
			}
			else{
				if(DEBUG)
					fprintf(stderr, "function--%s:%s is not defined for object:%s\n",newName,$7.name,$2.name );
				throwError(InvalidFunctionDefinition,newName);
				$8.type = typeError;
			}
		}
		else{
			if(DEBUG)
				fprintf(stderr, "Object:%s is undefined\n",$2.name );
			throwError(undefinedVar,$2.name);
			$8.type = typeError;
		}
	}
	subroutineBlock	{
		if($8.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	;
funcMarker:
	/*epsilon*/ {;}
	;
objFuncMarker:
	/*epsilon*/ {;}
	;
functionHeader:
	FUNCTION identifier parameterList COLON type 	{
		if( $5.type.id != typeError.id){
			$$.type = $5.type;
			$$.name = $2.name;
		}
		else{
			fprintf(stderr, "ERROR in functionHeader typeId is identifier,type:%d,%d\n",$3.type.id,$5.type.id );
			throwError(InvalidFunctionDefinition,$2.name);
			$$.type = typeError;
		}
	}
	;
parameterList:
	LPAREN RPAREN 	{
		actualArgumentList = NULL;
		$$.type = voidType;
	}
	|
	LPAREN {actualArgumentList = new argumentList;} parameterDeclarationList RPAREN 	{
		$$ = $3;
	}
	;
parameterDeclarationList:
	parameterDeclaration SEMICOLON parameterDeclarationList 	{
		$$ = $1;
	}
	|
	parameterDeclaration 	{
		$$ = $1;
	}
	;
parameterDeclaration:
	valueParameter 	{
		$$ = $1;
	}
	;
valueParameter:
	identifierList COLON parameterType 	{
		$$ = $3;
		if($3.type.id != typeError.id){
			for(auto i = identifierNameList->begin(); i != identifierNameList->end() ; i++){
				actualArgumentList->push_back({$$.type,*i});
			}
		}
		else{
			$$.type = typeError;
			fprintf(stderr, "ERROR in valueParameter-1 type is:%d\n",$3.type.id );
			throwError(InvalidFunctionDefinition);
		}
		delete identifierNameList;
		identifierNameList = new list<char*>;
	}
	|
	identifierList COLON ARRAY OF parameterType 	{
		$$ = $5;
		if($5.type.id != typeError.id){
			$$.type.level++;
			for(auto i = identifierNameList->begin(); i != identifierNameList->end() ; i++){
				actualArgumentList->push_back({$$.type,*i});
			}
		}
		else{
			$$.type= typeError;
			fprintf(stderr, "ERROR in valueParameter-2 type is:%d\n",$3.type.id );
			throwError(InvalidFunctionDefinition);
		}
		delete identifierNameList;
		identifierNameList = new list<char*>;
	}
	;/*
	identifier COLON parameterType EQUAL defaultParameterValue
	;*/
parameterType:
	CAP parameterType 	{
		$$ = $2;
		$$.type.level = $2.type.level +1;
	}
	|
	parameterTypeEnd {
		$$ = $1;
	}
	;
parameterTypeEnd:
	ordinalType 	{
		$$ = $1;
	}
	|
	STRING 	{
		$$.type = stringpType;
	}
	|
	realType 	{
		$$ = $1;
	}
	;
varReference:
	nonPointerReference 	{
		$$ = $1;
		if($$.type.id == typeError.id)
			throwError(undefinedVar);
	}
	|
	pointerReference 	{
		$$ = $1;
		if($$.type.id == typeError.id)
			throwError(undefinedVar);
	}
	;
nonPointerReference:
	indexedReference 	{
		$$ = $1;
	}
	|
	pointerReference foundDOT DOT pointerReference 	{
		if(exists($1.name) && $1.type.level == 0){
			if($1.type.childTable != NULL){
				auto tempTupleIt = $1.type.childTable->table.find(string($4.name));
				if(tempTupleIt != $1.type.childTable->table.end()){
					$4.type = tempTupleIt->second->type;
					$$.code.dest.intSrc = newTmp();
					$$.code.dest.multiplexer = ACtemp;
					$$.code.dest.type = $$.type = $4.type;
					tempThreeAC.tupleSrc = tempTupleIt->second;
					tempThreeAC.multiplexer = ACtuple;
					emit($$.code.dest,$1.code.dest,tempThreeAC,dotLoadOp);
					if(DEBUG) fprintf(stderr, "in nonPointerReference-2 \n");
				}
				else{
				   if(DEBUG) fprintf(stderr, "in nonPointerReference-2 -- \"%s\" not found in child Symbol Table of:%s is\n",$4.name,$1.type.name );
					printSymTable($1.type.childTable);
					$$.type = typeError;
					throwError(unknownObjectField,$1.type.name);
				}
			}
			else{
				if(DEBUG) fprintf(stderr, "in nonPointerReference-2 childTable for:%s with type:%s is NULL\n",$1.name,$1.type.name );
				$$.type = typeError;
				throwError(notObject,$1.name);
			}
		}
		else{
			if(DEBUG) fprintf(stderr, "in nonPointerReference-2 type.id:%d type.name:%s type.level:%d\n",$1.type.id,$1.type.name,$1.type.level );
			$$.type = typeError;
			throwError(undefinedVar,$1.name);
		}
		dotIsHere = false;
	}
	;
pointerReference:
	pointerReference CAP {
		if($1.type.id != typeError.id && $1.type.level >0 ){
			$$.type = $1.type;
			$$.type.level--;
			//	fprintf(stderr, "side is:%d yychar:%d CAP:%d\n",side,$1.yychar,CAP );
			// if(side == left && $1.yychar != CAP);
			// else{
				$$.code.dest.intSrc = newTmp();
				$$.code.dest.multiplexer = ACtemp;
				$$.code.dest.type = $$.type;
				emit($$.code.dest, $1.code.dest, nullThreeAC, loadOp);
			// }
			$$.isPointer = true;
			$$.name = $1.name;
		}
		else{
			$$.type = $$.code.dest.type = typeError;
			throwError(typeMismatch,"pointerReference");
		}
	}
	|
	identifier 	{
	//	fprintf(stderr, "In pointerReference:identfier\n" );
		if(exists($1.name) && $1.type.id != typeError.id){
			$$ = $1;
			$$.isPointer = false;
			$$.name = $1.name;
		}
		else if($1.type.id == typeError.id ){
			$$.type = $$.code.dest.type = typeError;
			if(DEBUG)
				fprintf(stderr, "IN pointerReference:identfier -- name:%s\tidentifier.type.id:%d\n",$1.name,$1.type.id);
			throwError(typeMismatch,$1.name);
		}
		else{
			$$.type = $$.code.dest.type = typeError;
			if(DEBUG)
				fprintf(stderr, "IN pointerReference:identfier -- name:%s\n",$1.name);
		}
	}
	;
indexedReference:
	arrayReference 	{
		$$ = $1;
	}
arrayReference:
	identifier LBRACKET 
		{
			if(exists($1.name)){
				arrayDimension = $1.type.level; 
				if(arrayDimension > 0){
					arrayRit = $1.code.dest.tupleSrc->type.rangeList->rbegin();
				}
				else{
					throwError(arrayIndexOverflow,"arrayReference-midRule");
				}
			}
			else{
				if(DEBUG)
					fprintf(stderr, "undefinedVar in arrayReference-midRule for \"%s\"\n",$1.name );
				throwError(undefinedVar,$1.name);
			}
		}
	 exprListArray RBRACKET 	{
			if($1.type.level>0)
			{	 		
				$$.type = $1.type;
				$$.type.level = arrayDimension;

				tempThreeAC.intSrc = newTmp();
				tempThreeAC.multiplexer = ACtemp;
				tempThreeAC.type = $$.type;
				ThreeAC_src tempThreeAC2,tempThreeAC3,tempThreeAC4;
				tempThreeAC3.multiplexer = ACtemp;
				tempThreeAC3.intSrc = newTmp();
				tempThreeAC3.type = $$.type;
				tempThreeAC4.multiplexer = ACint;
				tempThreeAC4.intSrc = 1;
				emit(tempThreeAC3,$4.code.dest,tempThreeAC4,plusOp);
				tempThreeAC2.multiplexer = ACint;
				tempThreeAC2.intSrc = $1.type.numBytes;
				emit(tempThreeAC,tempThreeAC2,tempThreeAC3,multiplyOp);

				$$.code.dest.intSrc = newTmp();
				$$.code.dest.multiplexer = ACtemp;
				$$.code.dest.type = $$.type;
				emit($$.code.dest,$1.code.dest,tempThreeAC,minusOp);
				// if(side == right){
					tempThreeAC.multiplexer = $$.code.dest.multiplexer;
					tempThreeAC.intSrc = $$.code.dest.intSrc;
					tempThreeAC.type = $$.type;
					$$.code.dest.intSrc = newTmp();
					$$.code.dest.multiplexer = ACtemp;
					$$.code.dest.type = $$.type;
					emit($$.code.dest,tempThreeAC,nullThreeAC,loadOp);
				// }
				$$.isPointer = true;
			}
			else{
				throwError(arrayIndexOverflow,"arrayReference-endRule");
				$$.type = typeError;
			}
	}
exprListArray:
	expr COMMA exprListArray    {
		if( $3.type.id != typeError.id && $1.type.id > typeError.id && $1.type.id <= int64Type.id ){
			if(arrayDimension > 0){
				$$.type = $1.type;
				ThreeAC_src tempThreeAC2,tempThreeAC3;
				tempThreeAC3.intSrc = newTmp();
				tempThreeAC3.multiplexer = ACtemp;
				tempThreeAC3.type = unsignedType;
				tempThreeAC.intSrc = arrayRit->start;
				tempThreeAC.multiplexer = ACint;
				emit(tempThreeAC3,$1.code.dest,tempThreeAC,minusOp);
				tempThreeAC.intSrc = newTmp();
				tempThreeAC.multiplexer = ACtemp;
				tempThreeAC.type = unsignedType;
				tempThreeAC2.multiplexer = ACint;
				tempThreeAC2.intSrc = arrayRit->size;
				emit(tempThreeAC,tempThreeAC3,tempThreeAC2,multiplyOp); 
				$$.code.dest.intSrc = newTmp();
				$$.code.dest.multiplexer = ACtemp;
				$$.code.dest.type = $$.type;
				emit($$.code.dest,tempThreeAC,$3.code.dest,plusOp);
				arrayRit++;
				arrayDimension--;
			}
			else{
				throwError(arrayIndexOverflow,"exprListArray");
				$$.type = typeError;
			}
		}
		else{
			throwError(typeMismatch,"exprListArray");
			$$.type = typeError;
		}
	}
	|
	expr   {
		if( $1.type.id > typeError.id && $1.type.id <= int64Type.id && $1.type.level == 0){
			if(arrayDimension > 0){
				$$.code.dest.intSrc = newTmp();
				$$.code.dest.multiplexer = ACtemp;
				$$.code.dest.type = $1.type;
				tempThreeAC.intSrc = arrayRit->start;
				tempThreeAC.multiplexer = ACint;
				emit($$.code.dest,$1.code.dest,tempThreeAC,minusOp);
				arrayRit++;
				arrayDimension--;
				$$.type = $1.type;
			}
			else{
				throwError(arrayIndexOverflow,"exprListArray:expr");
				$$.type = typeError;
			}
		}
		else{
			throwError(typeMismatch,"exprListArray-expr");
			$$.type = typeError;
		}
	}
	;
operatorDeclaration:
	OPERATOR assignmtDefinition 	{

	}
	|
	OPERATOR arithmeticDefinition 	{

	}
	|
	OPERATOR comparisonDefinition 	{

	}
	;
assignmtDefinition:
	CE LPAREN identifier/*argument variable*/ COLON parameterType /*type*/ RPAREN identifier/*return Variable*/ COLON parameterType /*return type*/ SEMICOLON initList compoundStatement SEMICOLON 	{

	}
	;
arithmeticDefinition:
	op2 LPAREN parameterList2 RPAREN identifier COLON parameterType SEMICOLON initList compoundStatement SEMICOLON 	{

	}
	;
comparisonDefinition:
	compOp LPAREN parameterList2 RPAREN identifier COLON BOOLEAN SEMICOLON initList compoundStatement SEMICOLON 	{

	}
	;
parameterList2:
	identifier COMMA identifier COLON parameterType 	{

	}
	|
	identifier COLON parameterType SEMICOLON identifier COLON parameterType 	{

	}
	|
	identifier COLON parameterType 	{

	}
	;
objectType:
	OBJECT 
	{ 
		newSymbolTable = new symTable;
		newSymbolTable->offset = 0;
		newSymbolTable->parent = currSymTable;
		newSymbolTable->typeList = new typeMap;
		newSymbolTable->returnVar = NULL;
		symStack.push(newSymbolTable);
	}
	componentList END        {

	}
	;
componentList:
	component componentList         {
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	|
	component       {
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	;
component:
	visibilitySpecifier { objectTupleList = new tupleList; } fieldOrMethodDefinitionList         
	{
		for(auto i = objectTupleList->begin(); i != objectTupleList->end(); i++)
			(*i)->isPrivate = $1.isPrivate;
		delete objectTupleList;
	}
	;
fieldOrMethodDefinitionList:
	fieldOrMethodDefinition fieldOrMethodDefinitionList     {
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}       
		else{
			if(DEBUG)
				fprintf(stderr, "ERROR in fieldOrMethodDefinitionList-1\n" );
		}
	}
	|
	fieldOrMethodDefinition         {
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
		else{
			if(DEBUG)
				fprintf(stderr, "ERROR in fieldOrMethodDefinitionList-2\n" );
		}
	}
	;
fieldOrMethodDefinition:
	fieldDefinition         {
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
		else{
			if(DEBUG)
				fprintf(stderr, "ERROR in fieldOrMethodDefinition-1\n" );
		}
	}
	|
	methodDefinition        {
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
		else{
			if(DEBUG)
				fprintf(stderr, "ERROR in fieldOrMethodDefinition-2\n" );
		}
	}
	;
fieldDefinition:
	identifierList COLON type SEMICOLON     {
		if($3.type.id != typeError.id){
			for(auto i = identifierNameList->begin(); i != identifierNameList->end() ; i++){
				objectTupleList->push_front(insertTuple($3.type,*i));
			}
			$$.type = voidType;
		}
		else{
			if(DEBUG)
				fprintf(stderr, "ERROR in fieldDefinition%s\n" );
			throwError(undefinedType,$3.name);
			$$.type = typeError;
		}
		delete identifierNameList;
		identifierNameList = new list<char*>;
	}
	;
methodDefinition:
	methodDefinitionHeader SEMICOLON        {
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
		else{
			if(DEBUG)
				fprintf(stderr, "ERROR in methodDefinition for %s\n",$1.name );
		}
	}
	;
methodDefinitionHeader:
	functionHeader  {
		if($1.type.id != typeError.id){
			if(DEBUG){
				fprintf(stderr, "In methodDefinitionHeader -- Inserting %s\n",$1.name );
			}
			$1.type.isObject = true;
			objectTupleList->push_front(insertTuple($1.type,$1.name,actualArgumentList));
			$$.type = voidType;
			$$.name = $1.name;
		}
		else{
			fprintf(stderr, "ERROR in methodDefinitionHeader-func\n");
			$$.type = typeError;
			$$.name = $1.name;
		//    throwError(InvalidFunctionDefinition,$1.name);
		}
	}
	|
	constructorHeader       {
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
		else{
			fprintf(stderr, "ERROR in methodDefinitionHeader-cons\n");
			$$.type = typeError;
			$$.name = $1.name;
		//    throwError(InvalidFunctionDefinition,$1.name);
		}
	}
	|
	destructorHeader        {
		if($1.type.id != typeError.id){
			$$.type = voidType;
		}
		else{
			fprintf(stderr, "ERROR in methodDefinitionHeader-des\n");
			$$.type = typeError;
			$$.name = $1.name;
		//    throwError(InvalidFunctionDefinition,$1.name);
		}
	}
	;
visibilitySpecifier:
	PRIVATE         {
		$$.isPrivate = true;
	}
	|
	PUBLIC  {
		$$.isPrivate = false;
	}
	;
constructorDeclaration:
	CONSTRUCTOR identifier DOT identifier parameterList SEMICOLON constructorDeclarationMarker
	{
		typeName tempType = findType($2.name);
		char* newName;
		if($5.type.id != typeError.id && tempType.id != typeError.id && tempType.isObject && tempType.childTable != NULL  ){
			symStack.push(tempType.childTable);
			newName = concat($4.name,actualArgumentList);
			if(actualArgumentList != NULL){
				delete actualArgumentList;
				actualArgumentList = NULL;
			}
			tupleNew* tempTuple = findTuple(newName);
			symStack.pop();
			if(tempTuple != NULL && tempTuple->type.id == constructorType.id && tempTuple->childTable != NULL){
				symStack.push(tempTuple->childTable);
				tempTuple->startIndex = nextIndex;
				tempThreeAC.multiplexer = ACtuple;
				tempThreeAC.tupleSrc = tempTuple;
				emit(tempThreeAC,nullThreeAC,nullThreeAC,enterOp);
			}
			else{
				if(DEBUG)
					fprintf(stderr, "function:%s is not a constructor for object:%s\n",newName,$2.name );
				throwError(InvalidConstructorDefinition,newName);
				$7.type = typeError;
			}
		}
		else{
			if(DEBUG)
				fprintf(stderr, "Object:%s is undefined\n",$2.name );
			throwError(undefinedVar,$2.name);
			$7.type = typeError;
		}
	}
	subroutineBlock   {
	   if($7.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	;
constructorDeclarationMarker:
	/*epsilon*/ { ;}
	;
destructorDeclaration:
	DESTRUCTOR identifier DOT identifier parameterList SEMICOLON destructorDeclarationMarker
	{
		typeName tempType = findType($2.name);
		char* newName;
		if($5.type.id != typeError.id && tempType.id != typeError.id && tempType.isObject && tempType.childTable != NULL  ){
			symStack.push(tempType.childTable);
			newName = concat($4.name,actualArgumentList);
			if(actualArgumentList != NULL){
				delete actualArgumentList;
				actualArgumentList = NULL;
			}
			tupleNew* tempTuple = findTuple(newName);
			symStack.pop();
			if(tempTuple != NULL && tempTuple->type.id == destructorType.id && tempTuple->childTable != NULL)
			{
				symStack.push(tempTuple->childTable);
				tempTuple->startIndex = nextIndex;
				tempThreeAC.multiplexer = ACtuple;
				tempThreeAC.tupleSrc = tempTuple;
				emit(tempThreeAC,nullThreeAC,nullThreeAC,enterOp);				
			}
			else{
				if(DEBUG)
					fprintf(stderr, "function:%s is not a destructor for object:%s\n",newName,$2.name );
				throwError(InvalidDestructorDefinition,newName);
				$7.type = typeError;
			}
		}
		else{
			if(DEBUG)
				fprintf(stderr, "Object:%s is undefined\n",$2.name );
			throwError(undefinedVar,$2.name);
			$7.type = typeError;
		}
	}
	subroutineBlock    {
		if($7.type.id != typeError.id){
			$$.type = voidType;
		}
	}
	;
destructorDeclarationMarker:
	/*epsilon*/ {;}
	;
constructorHeader:
	CONSTRUCTOR identifier /* can be a qualified method identfier*/ parameterList   {
		if($3.type.id != typeError.id){
			if(DEBUG){
				fprintf(stderr, "In constructorHeader -- Inserting %s\n",$2.name );
			}
			objectTupleList->push_front(insertTuple(constructorType,$2.name,actualArgumentList));
			$$.type = voidType;
		}
		else{
			fprintf(stderr, "ERROR in constructorHeader typeId is:%d\n",$3.type.id );
			throwError(InvalidConstructorDefinition,$2.name);
		}
	}
	;
destructorHeader:
	DESTRUCTOR identifier /* can be a qualified method identfier*/ parameterList    {
		if($3.type.id != typeError.id){
			if(DEBUG){
				fprintf(stderr, "In constructorHeader -- Inserting %s\n",$2.name );
			}
			objectTupleList->push_front(insertTuple(destructorType,$2.name,actualArgumentList));
			$$.type = voidType;
		}
		else{
			fprintf(stderr, "ERROR in destructorHeader typeId is:%d\n",$3.type.id );
			throwError(InvalidDestructorDefinition,$2.name);
		}
	}
	;
compOp:
	EQUAL   { $$.code.op = equalOp; }
	|
	LESS    { $$.code.op = lessOp; }
	|
	GREATER         { $$.code.op = grOp; }
	|
	LE      { $$.code.op = lessEqualOp; }
	|
	GE      { $$.code.op = grEqualOp; }
	|
	IN      { $$.code.op = inOp; }
	;
op2:
	PLUS    { $$.code.op = plusOp; }
	|
	MINUS   { $$.code.op = minusOp; }
	|
	MUL     { $$.code.op = multiplyOp;    }
	|
	DIVIDE  { $$.code.op = divideOp; }
	|
	SS      { $$.code.op = ssOp; }
	;
mulOp2:
	MUL     { $$.code.op = multiplyOp;$$.isComp = false; }
	|
	DIVIDE  { $$.code.op = divideOp;$$.isComp = false; }
	|
	DIV     { $$.code.op = intDivideOp;$$.isComp = false; }
	|
	MOD     { $$.code.op = modOp;$$.isComp = false; }
	|
	AND     { $$.code.op = andOp;$$.isComp = true; $$.isAndOr = true;}
	|
	AS      { $$.code.op = asOp;$$.isComp = false;}
	|
	SHL     { $$.code.op = shiftLeftOp;$$.isComp = false;}
	|
	SHR     { $$.code.op = shiftRightOp;$$.isComp = false;}
	|
	LL      { $$.code.op = shiftLeftOp;$$.isComp = false; }
	|
	GG      { $$.code.op = shiftRightOp;$$.isComp = false; }
	;
op1:
	LE      { $$.code.op = lessEqualOp; $$.isComp = true; $$.isAndOr = false;} /*added by me `MUL` was here too*/
	|
	GREATER         { $$.code.op = grOp;$$.isComp = true; $$.isAndOr = false;}
	|
	GE      { $$.code.op = grEqualOp;$$.isComp = true; $$.isAndOr = false; }
	|
	LESS    { $$.code.op = lessOp; $$.isComp = true; $$.isAndOr = false;}
	|
	EQUAL   { $$.code.op = equalOp;$$.isComp = true; $$.isAndOr = false;}
	|
	LG              { $$.code.op = notEqualOp;$$.isComp = true; $$.isAndOr = false;}
	|
	IN              { $$.code.op = inOp;$$.isComp = true; $$.isAndOr = false;}
	|
	IS              { $$.code.op = isOp;$$.isComp = true; $$.isAndOr = false;}
	;
addOp:
	PLUS    { $$.code.op = plusOp;$$.isComp = false;}
	|
	MINUS   { $$.code.op = minusOp;$$.isComp = false;}
	|
	OR              { $$.code.op = orOp;$$.isComp = true; $$.isAndOr = true;}
	|
	XOR     { $$.code.op = xorOp;$$.isComp = true; $$.isAndOr = false;}
	;
identifier:
	IDENTIFIER {
		if(exists($$.name)) {
			$$.code.dest.tupleSrc = findTuple($$.name);
			$$.code.dest.multiplexer = ACtuple;
			$$.type = $$.code.dest.type = $$.code.dest.tupleSrc->type;
		}
	} 
	;
%%
void yyerror(char *s) {
	 fprintf(stderr, "%s at line:%d \n",s,lineNo);
}

#include "assembly.cc"

int main(int argv, char *argc[]){
	bool print3ac = false;
	if(argv > 2){
		int abcd = atoi(argc[1]);
		if(abcd == 1){
			DEBUG = 1;
			DEBUG_ASM = 0;
		}
		else if(abcd == 2){
			DEBUG_ASM = 1;
			DEBUG = 0;
		}
		else if(abcd == 3){
			DEBUG = 1;
			DEBUG_ASM = 1;
		}
		else if(abcd == 4){
			print3ac = true;
		}
		else{
			printf("Incorrect Debug value\n");
			return 0;
		}
	}
	lineNo = 1;
	yyin = fopen(argc[argv-1],"r");
	if(yyin == NULL){
		printf("unable to open file %s\n",argc[argv-1] );
		return 0;
	}
	for (int i = 0; i < threeAClistLen; ++i){
		threeAClist[i].isLabel = false;
	}
	nullThreeAC.tupleSrc = NULL;
	nullThreeAC.intSrc = 0;
	nullThreeAC.strSrc = NULL;
	nullThreeAC.charSrc = '\0';
	nullThreeAC.boolSrc = 0;
	nullThreeAC.multiplexer = ACnone;
	nullThreeAC.type = typeError;
	tempThreeAC = nullThreeAC;
	mainTable = new symTable;
	mainTable->offset = 0;
	mainTable->parent = NULL;
	mainTable->typeList = new typeMap;
	mainTable->returnVar = NULL;
	symStack.push(mainTable);
	//insert pedefined types Do NOT change the order
	shortIntType = insertType("shortInt",2,0); //1
	unsignedType = insertType("unsigned",4,0); //2
	integerType = insertType("integer",4,0); //3
	unsignedLongType = insertType("unsignedLong",8,0);
	int64Type = insertType("int64",8,0);
	floatType = insertType("float",4,0);
	doubleType = insertType("double",8,0);
	charType = insertType("char",1,0);
	booleanType = insertType("boolean",1,0);
	stringpType = insertType("stringP",0,1);
	rangePType = insertType("range",4,0);
	voidType = insertType("voidType",4,0);
	nilType = insertType("nil", 1,0);
	objectPType = insertType("objectType",0,0);
	constructorType = insertType("constructor",0,0);
	destructorType = insertType("destructor",0,0);
	constructorType.isObject = destructorType.isObject = true;
	// yydebug = 1;
	yyparse();
	fclose(yyin);

	// Machine Code Generation
	int i;
	char *outFileName = new char [105];
	for(i=0;(argc[1][i] != '\0') && (argc[1][i] != '.') && (i < 100) ;i++){
		outFileName[i] = argc[1][i];
	}
	outFileName[i] = '\0';
	strcat(outFileName,".s\0"); 
	if(DEBUG_ASM)
		print3AClist();
	genAssemblyCode(outFileName);
	if(print3ac)
		print3AClist() ; 
	if(gotError)
		fprintf(stderr, "ERROR !!\n" );
	// printSymTable(mainTable);
	return 0;
}
