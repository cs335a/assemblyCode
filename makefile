SHELL := /bin/bash
.PHONY: example assign2 calc assign2_DEBUG
.DEFAULT_GOAL := assign2
example:
	mkdir -p bin
	flex -o src/example1.yy.c src/example1.l
	cc src/example1.yy.c -o bin/example1 -ll

assign2_DEBUG:
	mkdir -p bin
#	sed -i 's@//yydebug = 1@yydebug = 1@' src/assign2.y
	flex -o src/assign2.yy.c src/assign2.l
	bison -d -g --debug --verbose -o src/assign2.tab.c src/assign2.y
	g++-4.8 -g -std=c++11 src/assign2.yy.c src/assign2.tab.c -lfl -o bin/compiler

assign2:
	mkdir -p bin
#	sed  -i 's@\tyydebug = 1@\t//yydebug = 1@' src/assign2.y
	flex -o src/assign2.yy.c src/assign2.l
	bison  -d -o src/assign2.tab.c src/assign2.y
	g++-4.8 -std=c++11 -w src/assign2.yy.c src/assign2.tab.c -lfl -o bin/compiler

clean:
	rm -rf bin
	find -name *.out | xargs /bin/rm -f

cleanall:  clean
	git clean -f -X
#	@echo "-------------------------------"
#	@echo "Are you sure:(y/n)"
#	@read -p "Are you sure:(y/n)" c; \
#	ifeq $$c "y"	git clean -f -X; 	endif

calc:
	mkdir -p bin
	flex -o example/calc.yy.c example/calc.l
	bison -d -o example/calc.tab.c example/calc.y
	cc example/calc.yy.c example/calc.tab.c -lfl -o bin/calc

gen-asm:
	gcc-4.8 -S -m32 $(target).c -o $(target).s -masm=intel
	gcc-4.8 -m32 $(target).s -o $(target)
