program insertion;
var
a : array[1..100] of integer;
i, n : integer;
type
	vector = array[1..100] of integer;
function insertion_sort(a:vector;n:integer):integer;
var
	i, pos : integer;
	value : integer;
	done : integer;
begin
	for i := 2 to n do
	begin
		value := a[i];
		pos := i;
		done := 0;
		while (done = 0) do
			begin
			if pos <= 1 then
				done := 1
			else if (value >= a[pos-1]) then
			begin
				done := 1;
			end
			else
			begin
				a[pos] := a[pos-1];
				pos := pos-1;
			end;
		end; {while}

		a[pos] := value;

	end; {for}
end;

var 
	temp:integer;

begin { main }
write('How many numbers would you like to sort:');
read('%d',@n);

write('Enter the numbers:');
for i := 1 to n do
begin
	read('%d',@temp);
	a[i] := temp;
end;

insertion_sort(a,n);

for i := 1 to n do
write('%d ',a[i]);
write('\n');
end.