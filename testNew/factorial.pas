program FindFactorial;
var 
	i, ans : integer;

function Factorial(n : Integer) : Integer;
begin
	if(n = 1)then 
	begin
		Factorial := 1; 
	end
	else
	begin
		Factorial := n*Factorial(n-1);
	end;
end;

begin
	write('Enter a number:');
	read('%d', @i);
	ans := Factorial(i);
	write('Factorial of %d is %d.\n',i, ans);
end.