program Swapping;
var 
	a,b : integer;

function Swap(p,q : ^Integer) : Integer;
var
    temp : integer;
begin
	temp := p^;
	p^ := q^;
	q^ := temp;
end;

begin
	a := 1;
	b := 2;
	write('Before Swapping\n');
	write('Enter a: ');
	read('%d', @a);
	write('Enter b: ');
	read('%d', @b);
	Swap(@a,@b);
	write('After Swapping\n');
	write('a = %d\nb = %d\n',a,b);
end.