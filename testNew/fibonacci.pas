program recursiveFibonacci;
var
   i,res,n: integer;
function fibonacci(n: integer): integer;
begin
   if n=1 then
      fibonacci := 0
   else if n=2 then
      fibonacci := 1
   else
      fibonacci := fibonacci(n-1) + fibonacci(n-2);
end; 
begin
   write('Enter a number:');
   read('%d',@n);
   write('%dth Fibonacci series is\n',n);
   for i := 1 to n do
   begin
      res := fibonacci(i);
      write('%d ',res);
   end;
   write('\n');
end.