program overload2;
function print(a:integer):integer;
begin
	write('a is:%d\n',a);
end;
function print(a:real):integer;
begin
	write('a is:%f\n',a);
end;

var 
	a:integer;
	b:real;
begin
	a := 1;
	b := 0.4;
	print(a);
	print(b);
end.