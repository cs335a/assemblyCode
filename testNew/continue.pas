program BrCo;
var
  i,j,input : integer;
begin
  write('Enter a number:');
  read('%d',@input);
  for j:= 1 to 10000 do
  begin
    if(input = j) then
    begin
    	write('Breaking at counter = %d\n',j);
    	break;
    end;
    write('Loop counter is %d\n',j);
  end;
  write('Enter another number:');
  read('%d',@input);
  for j:= 1 to 100000 do
  begin
    if(input = j) then
    begin
      write('looping at counter = %d\n',j);
      continue;
    end;
    write('Loop counter is %d\n',j);
  end;
end.