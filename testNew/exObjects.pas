program exObjects;
type
   Rectangle = object  
   public  
      length, width,area: integer;
end;
var
  r1: Rectangle;
  a,b:integer;
  
begin
	write('Enter length and width:');
	read('%d%d',@a,@b);
    r1.length :=a;
    r1.width := b;
    r1.area := r1.length*r1.width;
    write('Area of a %d x %d Rectangle is:%d\n',r1.length,r1.width, r1.area);
end.