program enum;
type 
	color = (red,green := 3,blue);
var
	colour:color;
begin
	colour := red;
	write('color is :%d\n',colour);
	colour := green;
	write('color is :%d\n',colour);
	colour := blue;
	write('color is :%d\n',colour);
end.
