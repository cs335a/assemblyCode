program pointer;

var b : ^^integer;
	c : ^integer;
	d : integer;
function pointerFun(b:^^integer):integer;
begin 
	b^^ := 10;
end;

var 
	i:integer;
begin
	b := @c;
	c := @d;
	d := 1;
	write('Before calling function b is %d\n',b^^);
	pointerFun(b);
	write('After calling function -- b is %d\n',b^^);
end.