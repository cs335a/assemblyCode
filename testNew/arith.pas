program arith;
var 
	a,b,sum,prod,diff:integer;
	division:real;

begin
	write('Enter two numbers:');
	read('%d%d',@a,@b);
	sum := a + b;
	prod := a*b;
	diff := a-b;
	division := a/b; 
	if(a < b) then
	begin
		write('%d is less than %d\n',a,b);
	end
	else if( a > b) then
	begin
		write('%d is greater than %d\n',a,b);
	end
	else
	begin
		write('%d is equal to %d\n',a,b);
	end;
	write('sum is:%d\ndifference is:%d\nproduct is:%d\n',sum,diff,prod);
	write('division is:%f\n',division);
end.